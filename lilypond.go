package lilypond

import (
	"fmt"
	"os"
)

type Command struct {
	Help    bool
	Version bool
	Verbose bool
	Silent  bool

	Relocate bool

	DefineDefault string
	Evaluate      string
	Formats       string
	Header        string
	Include       string
	Init          string
	Jail          string
	LogLevel      string

	Output       string
	LilypondFile string
}

func (m Command) Run() error {
	//fmt.Println(strings.Join(m.Args().Strings(), " "))
	cmd := cmdLilypond(m.LilypondFile, m.Args().Strings())
	//return cmd.Run()
	out, err := cmd.CombinedOutput()
	fmt.Fprintf(os.Stdout, string(out))
	return err
}

func (m Command) Args() (a args) {

	if m.Version {
		a = append(a, arg{"--version", true})
	}

	if m.Help {
		a = append(a, arg{"--help", true})
	}

	if m.Silent {
		a = append(a, arg{"--silent", true})
	}

	if m.Relocate {
		a = append(a, arg{"--relocate", true})
	}

	if m.DefineDefault != "" {
		a = append(a, arg{"--define-default", m.DefineDefault})
	}

	if m.Evaluate != "" {
		a = append(a, arg{"--evaluate", m.Evaluate})
	}

	if m.Formats != "" {
		a = append(a, arg{"--formats", m.Formats})
	}

	if m.Header != "" {
		a = append(a, arg{"--header", m.Header})
	}

	if m.Include != "" {
		a = append(a, arg{"--include", m.Include})
	}

	if m.Init != "" {
		a = append(a, arg{"--init", m.Init})
	}

	if m.Jail != "" {
		a = append(a, arg{"--jail", m.Jail})
	}

	if m.LogLevel != "" {
		a = append(a, arg{"--loglevel", m.LogLevel})
	}

	if m.Output != "" {
		a = append(a, arg{"--output", m.Output})
	}

	return

}

/*
Aufruf: lilypond [OPTION]... DATEI...

Musiksatz und/oder MIDI aus DATEI erzeugen.

LilyPond erzeugt ansprechenden Notensatz.
Für weitere Informationen siehe http://lilypond.org

Optionen:
  -d, --define-default=SYM[=WERT]          Scheme-Option SYM auf WERT setzen (Vorgabe: #t).
                                             -dhelp für Hilfe verwenden.
  -e, --evaluate=AUSD                      Scheme-Code auswerten
  -f, --formats=FORMATe                    dump FORMAT,... Auch als separate Optionen:
      --pdf                                PDF erzeugen (Standard)
      --png                                PNG erzeugen
      --ps                                 PostScript erzeugen
  -h, --help                               diese Hilfe anzeigen und beenden
  -H, --header=FELD                        Header-Feld FELD in Datei BASISNAME.FELD schreiben
  -I, --include=VERZ                       VERZ zum Suchpfad hinzufügen
  -i, --init=DATEI                         DATEI als Anfangsdatei verwenden
  -j, --jail=BENUTZER,GRUPPE,KERKER,VERZ   chroot in KERKER, wird BENUTZER:GRUPPE
                                             und cd in VERZ
  -l, --loglevel=LOGLEVEL                  Logmeldungen nach LOGLEVEL-Einstellung ausgeben.  Mögliche Werte:
                                             NONE, ERROR, WARNING, BASIC, PROGRESS, INFO (Standard) und DEBUG.
  -o, --output=DATEI                       Ausgabe in DATEI schreiben (Endung wird hinzugefügt)
      --relocate                           wiederfinden mit Hilfe des Lilypond-Programmverzeichnisses
  -s, --silent                             kein Forschritt, nur Fehlermeldungen (entspricht LOGLEVEL=ERROR)
  -v, --version                            Versionsnummer ausgeben und beenden
  -V, --verbose                            ausführlich sein (entspricht LOGLEVEL=DEBUG)
  -w, --warranty                           Informationen zu Gewährleistung und Copyright anzeigen

*/
