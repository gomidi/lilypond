package lilypond

import (
	"bytes"
	"fmt"
)

type Lyrics struct {
	elements []Element
	lyricsTo string
	with     []Element
}

func (v *Lyrics) With(s ...Element) *Lyrics {
	v.with = append(v.with, s...)
	return v
}

func (l *Lyrics) Add(elms ...Element) *Lyrics {
	l.elements = append(l.elements, elms...)
	return l
}

func (v Lyrics) String() string {
	var bf bytes.Buffer
	bf.WriteString(`\new Lyrics ` + "\n")

	if v.lyricsTo != "" {
		// \lyricsto "myRhythm" {
		fmt.Fprintf(&bf, `\lyricsto %q `+"\n", v.lyricsTo)
	}

	if len(v.with) > 0 {
		fmt.Fprintf(&bf, ` \with { `+"\n")

		for _, w := range v.with {
			bf.WriteString(w.String())
		}

		bf.WriteString("\n } \n")
	}

	bf.WriteString(" { \n")

	bf.WriteString(`\lyricmode { ` + "\n")

	for _, s := range v.elements {
		bf.WriteString(s.String())
	}

	bf.WriteString("\n } \n")
	bf.WriteString("\n } \n")

	return bf.String()
}

/*
Lyrics

Eingabe von Text
Gesangstext muss in einem speziellen Modus notiert werden. Der Gesangstextmodus kann mit der Umgebung \lyricmode angegeben werden, oder indem \addlyrics bzw. \lyricsto eingesetzt wird. In diesem Modus kann Text mit Akzenten und Satzzeichen notiert werden, und das Programm liest d nicht als die Tonhöhe D, sondern als eine Silbe Text. Anders gesagt: Silben werden wie Noten notiert, aber die Tonhöhen werden durch Text ersetzt.

Beispielsweise:

\lyricmode { Gern4 hätt’4 ich4 dich4 lieb!2 }
Es gibt zwei generelle Methoden, die horizontale Orientierung der Textsilben anzugeben, entweder indem ihre Dauer angegeben wird, wie oben in dem Beispiel, oder indem die Silben automatisch an den Noten ausgerichtet werden. Dazu muss entweder \addlyrics oder \lyricsto eingesetzt werden. Die erste Methode ist beschrieben in Manuelle Silbendauern, die zweite in Automatische Silbendauern.

Ein Wort oder eine Silbe beginnt mit einem alphabetischen Zeichen (inklusive einige andere Zeichen, siehe unten) und endet mit einem Leerzeichen oder einer Zahl. Die folgenden Zeichen in der Silbe können beliebig sein, außer Leerzeichen und Zahlen.

Jedes Zeichen, das nicht Leerzeichen noch Zahl ist, wird als Bestandteil der Silbe angesehen. Eine Silbe kann also auch mit } enden, was oft zu dem Fehler

\lyricmode { lah- lah}
führen kann. Hier wird } als Teil der letzten Silbe gerechnet, so dass die öffnende Klammer keine schließende Klammer hat und die Eingabedatei nicht funktioniert. Klammern sollten deshalb immer von Leerzeichen umgeben sein.

\lyricmode { lah lah lah }
Auch ein Punkt, der auf eine Silbe folgt, wird in die Silbe inkorporiert. Infolgedessen müssen auch um Eigenschaftsbezeichnungen Leerzeichen gesetzt werden. Ein Befehl heißt also nicht:

\override Score.LyricText.font-shape = #'italic
sondern

\override Score.LyricText.font-shape = #'italic
Punkte, Gesangstext mit Akzenten, verschiedene lateinische und nicht-lateinische Zeichen sowie auch etwa Sonderzeichen (wie ein Herz-Symbol) können direkt in die Notationsdatei geschrieben werden. Es muss dabei sichergestellt werden, dass die Datei in der UTF-8-Kodierung gespeichert wird. Zu mehr Information siehe Sonderzeichen.

\relative { d''8 c16 a bes8 f e' d c4 }
\addlyrics { „Schad’ um das schö -- ne grü -- ne Band, }

Normale Anführungszeichen können im Gesangstext auch benutzt werden, aber sie müssen mit einem Backslash und weiteren Anführungszeichen begleitet werden:

\relative { \time 3/4 e'4 e4. e8 d4 e d c2. }
\addlyrics { "\"I" am so lone -- "ly,\"" said she }

mel = \relative c'' { c4 c c c }
lyr = \lyricmode {
  Lyrics \markup { \italic can } \markup { \with-color #red contain }
  \markup { \fontsize #8 \bold Markup! }
}

<<
  \new Voice = melody \mel
  \new Lyrics \lyricsto melody \lyr
>>

<<
  \new Staff <<
    \time 2/4
    \new Voice = "one" \relative {
      \voiceOne
      c''4 b8. a16 g4. r8 a4 ( b ) c2
    }
    \new Voice = "two" \relative {
      \voiceTwo
       s2 s4. f'8 e4 d c2
    }
  >>

% takes durations and alignment from notes in "one"
  \new Lyrics \lyricsto "one" {
    Life is __ _ love, live __ life.
  }

% takes durations and alignment from notes in "one" initially
% then switches to "two"
  \new Lyrics \lyricsto "one" {
    No more let
    \set associatedVoice = "two"  % must be set one syllable early
    sins and sor -- rows grow.
  }
>>

Gesangstext kann unabhängig von den Notendauern platziert werden, indem man die Dauern der Silben explizit vorgibt und den Text innerhalb von \lyricmode notiert:
<<
  \new Voice = "one" \relative {
    \time 2/4
    c''4 b8. a16 g4. f8 e4 d c2
  }

% uses previous explicit duration of 2;
  \new Lyrics \lyricmode {
    Joy to the earth!
  }

% explicit durations, set to a different rhythm
  \new Lyrics \lyricmode {
    Life4 is love,2. live4 life.2
  }
>>

Die erste Strophe wird nicht an den Noten ausgerichtet, weil die Dauern nicht angegeben sind, und der erste Wert 2 für alle Silben benutzt wird.

Die zweite Strophe zeigt, dass die Silben sehr unabhängig von den Noten notiert werden können. Das ist nützlich, wenn der Text von verschiedenen Strophen die Noten auf unterschiedliche Weise füllt, aber die Dauer nicht in einem Noten-Kontext vorhanden ist. Mehr Details finden sich in Manuelle Silbendauern.



Automatische Silbendauern
Die Silben des Gesangstextes können automatisch an einer Melodie ausgerichtet werden. Das ist auf drei Arten möglich:

indem man einen benannten Voice-Kontext mit der Melodie durch \lyricsto zuweist,
indem man den Gesangstext mit \addlyrics beginnt und direkt nach dem Voice-Kontext mit der Melodie notiert,
indem man die associatedVoice-Eigenschaft definiert, sodass man die Ausrichtung des Gesangstextes zwischen verschiedenen benannten Voice-Kontexten gewechselt werden.
In allen drei Methoden können Bindestriche zwischen den Silben oder Fülllinien hinter einem Wortende gezogen werden. Zu Einzelheiten siehe Fülllinien und Trennstriche.

*/

/*
Mehrstimmigkeit in einem System
Stimmen explicit beginnen

Die grundlegende Struktur, die man benötigt, um mehrere unabhängige Stimmen in einem Notensystem zu setzen, ist im Beispiel unten dargestellt:

\new Staff <<
  \new Voice = "first"
    \relative { \voiceOne r8 r16 g'' e8. f16 g8[ c,] f e16 d }
  \new Voice= "second"
    \relative { \voiceTwo d''16 c d8~ 16 b c8~ 16 b c8~ 16 b8. }
>>

Der Voice-Kontext, der die Melodie enthält, an der der Text ausgerichtet werden soll, darf noch nicht „gestorben“ sein, weil sonst aller Text danach verloren geht. Das kann passieren, wenn es Strecken gibt, in denen die Stimme pausiert. Zu Methoden, wie man Kontexte am Leben erhält, siehe Kontexte am Leben halten.

\lyricsto Benutzen
Gesangstext kann an einer Melodie automatisch ausgerichtet werdne, indem man den beannten Voice-Kontext mit der Melodie durch den Befehl \lyricsto angibt:

<<
  \new Voice = "melody" {
    a4 a a a
  }
  \new Lyrics \lyricsto "melody" {
    These are the words
  }
>>

Damit wird der Text an den Noten des benannten Voice-Kontextes ausgerichtet, der schon vorher existieren muss. Aus diesem Grund wird der Voice-Kontext normalerweise zuerst definiert, gefolgt vom Lyrics-Kontext. Der Gesangstext selber folgt dem \lyricsto-Befehl. Der \lyricsto-Befehl ruft den Gesangstextmodus automatisch auf, sodass man \lyricmode in diesem Fall auslassen kann. Standardmäßig werden die Silben unter den Noten angeordnet. Für andere Optionen siehe

\addlyrics benutzen
Der \addlyrics-Befehl ist eigentlich nur eine Abkürzung für eine etwas kompliziertere LilyPond-Struktur, den man manchmal aus Bequemlichkeit einsetzen kann.

{ Noten }
\addlyrics { Gesangstext }
bedeutet das Gleiche wie

\new Voice = "bla" { Noten }
\new Lyrics \lyricsto "bla" { Gesangstext }
Hier ein Beispiel:

{
  \time 3/4
  \relative { c'2 e4 g2. }
  \addlyrics { play the game }
}

Weitere Strophen können mit weiteren \addlyrics-Abschnitten hinzugefügt werden:

{
  \time 3/4
  \relative { c'2 e4 g2. }
  \addlyrics { play the game }
  \addlyrics { speel het spel }
  \addlyrics { joue le jeu }
}

Der Befehl \addlyrics kann keine polyphonen Situationen bewältigen. In diesen Fällen sollen man \lyricsto benutzen.

associatedVoice benutzen
Die Melodie, an die der Gesangstext ausgerichtet wird, kann durch Setzen der associatedVoice-Eigenschaft geändert werden:

\set associatedVoice = "lala"
Der Wert der Eigenschaft (hier "lala") ist die Bezeichnung eines Voice-Kontextes. Aus technischen Gründen muss der \set-Befehl eine Silbe vor der Silbe gesetzt werden, auf die er wirken soll.

Ein Beispiel demonstiert das:

<<
  \new Staff <<
    \time 2/4
    \new Voice = "one" \relative {
      \voiceOne
      c''4 b8. a16 g4. r8 a4 ( b ) c2
    }
    \new Voice = "two" \relative {
      \voiceTwo
       s2 s4. f'8 e8 d4. c2
    }
  >>
% takes durations and alignment from notes in "one" initially
% then switches to "two"
  \new Lyrics \lyricsto "one" {
    No more let
    \set associatedVoice = "two"  % must be set one syllable early
    sins and sor -- rows grow.
  }
>>

Manuelle Silbendauern
In komplexer Vokalmusik kann es nötig sein, den Gesangstext vollkommen unabhängig von den Noten zu positionieren. In diesem Fall sollte man nicht \addlyrics bzw. \lyricsto benutzen, und auch keine associatedVoice definieren. Die Silben werden wie Noten notiert – indem die Tonhöhen durch den Text der Silbe ersetzt werden – und die Dauer jeder Silbe muss angegeben werden.

Standardmäßig werden die Silben links am entsprechenden musikalischen Moment ausgerichtet. Bindestriche zwischen den Silben können wie üblich gezogen werden, aber Fülllinien hinter dem Wortende können nicht gezogen werden, wenn es keine mit dem Text verknüpfte Melodie gibt.

Hier zwei Beispiele:

<<
  \new Voice = "melody" {
    \time 3/4
    c2 e4 g2 f
  }
  \new Lyrics \lyricmode {
    play1 the4 game4
  }
>>

<<
  \new Staff {
    \relative {
      c''2 c2
      d1
    }
  }
  \new Lyrics {
    \lyricmode {
      I2 like4. my8 cat!1
    }
  }
  \new Staff {
    \relative {
      c'8 c c c c c c c
      c8 c c c c c c c
    }
  }
>>

Mehrere Silben zu einer Note
Um mehr als eine Silbe zu einer Note zuzuordnen, können die Silben mit geraden Anführungszeichen (") umgeben werden oder ein Unterstrich (_) benutzt werden, um ein Leerzeichen zwischen Silben zu setzen. Mit der Tilde (~) kann ein Bindebogen gesetzt werden.

{
  \relative { \autoBeamOff
    r8 b' c fis, fis c' b e, }
  \addlyrics { Che_in ques -- ta_e_in quel -- l'al -- tr'on -- da }
  \addlyrics { "Che in" ques -- "ta e in" quel -- l'al -- tr'on -- da }
  \addlyrics { Che~in ques -- ta~e~in quel -- l'al -- tr'on -- da }
}

Mehrere Noten zu einer Silbe
Öfters, insbesondere in Alter Musik, wird eine einzige Silbe zu mehreren Noten gesungen, was als Melisma bezeichnet wird. Die Silbe eines Melismas wird normalerweise links an der ersten Note des Melismas ausgerichtet.

Melismen können direkt im Gesangstext definiert werden, indem ein Unterstrich (_) für jede Note notiert wird, die übersprungen werden soll.

Wenn ein Melisma an einer Silbe auftritt, die nicht die letzte eines Wortes ist, wird diese Silbe mit der folgenden durch wiederholte Trennstriche verbunden. Dies wird notiert, indem man zwei Trennstriche (--) nach der Silbe notiert.

Wenn ein Melisma an der letzten Silbe eines Wortes auftritt, wird eine Fülllinie vom Ende der Silbe bis zur letzten Note des Melismas gezeichnet. Das wird durch zwei Unterstriche (__) nach der Silbe notiert.

Es gibt fünf Arten, auf die ein Melisma angezeigt werden kann:

Melismen werden automatisch zu Noten erstellt, die übergebunden sind:
<<
  \new Voice = "melody" \relative {
    \time 3/4
    f''4 g2 ~ |
    4 e2 ~ |
    8
  }
  \new Lyrics \lyricsto "melody" {
    Ky -- ri -- e __
  }
>>

Melismen können automatisch aus den Noten erstellt werden, indem man Legatobögen über den Noten eines Melismas notiert. Auf diese Weise wird Gesangstext üblicherweise notiert:
<<
  \new Voice = "melody" \relative {
    \time 3/4
    f''4 g8 ( f e f )
    e8 ( d e2 )
  }
  \new Lyrics \lyricsto "melody" {
    Ky -- ri -- e __
  }
>>

Noten werden als ein Melisma betrachtet, wenn sie manuell mit einem Balken versehen werden, vorausgesetzt, dass die automatische Bebalkung ausgeschaltet ist. Siehe Einstellung von automatischen Balken.
<<
  \new Voice = "melody" \relative {
    \time 3/4
    \autoBeamOff
    f''4 g8[ f e f]
    e2.
  }
  \new Lyrics \lyricsto "melody" {
    Ky -- ri -- e
  }
>>

Eine Gruppe von Noten ohne Legatobogen werden als Melisma betrachtet, wenn sie zwischen \melisma und \melismaEnd eingeschlossen sind:
<<
  \new Voice = "melody" \relative {
    \time 3/4
    f''4 g8
    \melisma
    f e f
    \melismaEnd
    e2.
  }
  \new Lyrics \lyricsto "melody" {
    Ky -- ri -- e
  }
>>

Ein Melisma kann auch ausschließlich im Gesangstext notiert werden, indem man einzlene Unterstriche (_) für jede Note eingibt, die zum Melisma hinzugefügt werden soll.
<<
  \new Voice = "melody" \relative {
    \time 3/4
    f''4 g8 f e f
    e8 d e2
  }
  \new Lyrics \lyricsto "melody" {
    Ky -- ri -- _ _ _ e __ _ _
  }
>>

Man kann durchaus auch Binde- und Legatobögen sowie manuelle Balken benutzen, ohne dass sie Melismen bezeichnen, wenn melismaBusyProperties aufgerufen wird:

<<
  \new Voice = "melody" \relative {
    \time 3/4
    \set melismaBusyProperties = #'()
    c'4 d ( e )
    g8 [ f ] f4 ~ 4
  }
  \new Lyrics \lyricsto "melody" {
    Ky -- ri -- e e -- le -- i -- son
  }
>>

Wenn ein Melisma während einer Passage benötigt wird, in der melismaBusyProperties aktiviert ist, kann es durch einen einzelnen Unterstrich im Gesangstext für jede Note des Melismas angegeben werden:

<<
  \new Voice = "melody" \relative {
    \time 3/4
    \set melismaBusyProperties = #'()
    c'4 d ( e )
    g8 [ f ] ~ 4 ~ f
  }
  \new Lyrics \lyricsto "melody" {
    Ky -- ri -- _ e __ _ _ _
  }
>>

Vordefinierte Befehle
\autoBeamOff, \autoBeamOn, \melisma, \melismaEnd.

Fülllinien und Trennstriche
Wenn die letzte Silbe eines Wortes auf ein Melisma fällt, wird das Melisma oft mit einer langen horizontalen Linie angezeigt, die nach dem Wort beginnt und mit der letzten Note des Melismas endet. Derartige Fülllinien werden mit einem doppelten Unterstrich ( __ ) eingegeben, wobei beachtet werden muss, dass er von Leerzeichen umgeben ist.

Achtung: Melismen werden mit Fülllinien angezeigt, die als doppelter Unterstrich notiert sind. Kurze Melismen können auch notiert werden, indem eine Note übersprungen wird. Hierzu wird ein einfacher Unterstrich notiert und keine Fülllinie gezogen.

Zentrierte Bindestriche zwischen den einzelnen Silben werden mit einem doppelten Bindestrich ( -- ) eingegeben, wobei beachtet werden muss, dass er von Leerzeichen umgeben ist. Der Bindestrich wird zwischen den Silben zentriert und seine Länge dem Notenabstand angepasst.

In sehr eng notierter Musik können die Bindestriche ganz wegfallen. Dieses Verhalten kann aber auch unterbunden werden, wenn den Eigenschaften minimum-distance (minimaler Abstand zwischen Silben) und minimum-length (Wert, unterhalb von dem Bindestriche wegfallen) andere Werte erhalten. Beide sind Eigenschaften von LyricHyphen.

*/

/*

Das nächste Beispiel zeigt eine Melodie, die zeitweise unterbrochen wird und wie man den entsprechenden Gesangstext mit ihr verknüpfen kann, indem man die Stimme am Leben hält. In wirklichen Situationen würden Begleitung und Melodie natürlich aus mehreren Abschnitten bestehen.

melody = \relative { a'4 a a a }
accompaniment = \relative { d'4 d d d }
words = \lyricmode { These words fol -- low the mel -- o -- dy }
\score {
  <<
    \new Staff = "music" {
      <<
        \new Voice = "melody" {
          \voiceOne
          s1*4  % Keep Voice "melody" alive for 4 bars
        }
        {
          \new Voice = "accompaniment" {
            \voiceTwo
            \accompaniment
          }
          <<
            \context Voice = "melody" { \melody }
            \context Voice = "accompaniment" { \accompaniment }
          >>
          \context Voice = "accompaniment" { \accompaniment }
          <<
            \context Voice = "melody" { \melody }
            \context Voice = "accompaniment" { \accompaniment }
          >>
        }
      >>
    }
    \new Lyrics \with { alignAboveContext = "music" }
    \lyricsto "melody" { \words }
  >>
}

Eine Alternative, die in manchen Umständen besser geeignet sein kann, ist es, einfach unsichtbare Pausen einzufügen, um die Melodie mit der Begleitung passend auszurichten:

melody = \relative {
  s1  % skip a bar
  a'4 a a a
  s1  % skip a bar
  a4 a a a
}
accompaniment = \relative {
  d'4 d d d
  d4 d d d
  d4 d d d
  d4 d d d
}
words = \lyricmode { These words fol -- low the mel -- o -- dy }

\score {
  <<
    \new Staff = "music" {
      <<
        \new Voice = "melody" {
          \voiceOne
          \melody
        }
        \new Voice = "accompaniment" {
          \voiceTwo
          \accompaniment
        }
      >>
    }
    \new Lyrics \with { alignAboveContext = "music" }
    \lyricsto "melody" { \words }
  >>
}

Strophen mit unterschiedlichem Rhythmus
Oft haben unterschiedliche Strophen eines Liedes leicht unterschiedliche Silbenzahlen und werden darum auf andere Art zur Melodie gesungen. Derartige Variationen können mit \lyricsto bewältigt werden.

Melismen ignorieren
Teilweise wird zu einer Silbe ein Melisma in einer Strophe gesungen, während in einer anderen jede Note eine Silbe erhält. Eine Möglichkeit ist, dass die Strophe mit mehr Text das Melisma ignoriert. Das wird mit der ignoreMelismata-Eigenschaft im Lyrics-Kontext vorgenommen.

<<
  \relative \new Voice = "lahlah" {
    \set Staff.autoBeaming = ##f
    c'4
    \slurDotted
    f8.[( g16])
    a4
  }
  \new Lyrics \lyricsto "lahlah" {
    more slow -- ly
  }
  \new Lyrics \lyricsto "lahlah" {
    go
    \set ignoreMelismata = ##t
    fas -- ter
    \unset ignoreMelismata
    still
  }
>>

Silben zu Verzierungsnoten hinzufügen
Normalerweise werden Verzierungsnoten (z.B. durch \grace) bei \lyricsto keine Silben zugeordnet. Dieses Verhalten kann geändert werden, wie das folgende Beispiel zeigt.

<<
  \new Voice = melody \relative {
    f'4 \appoggiatura a32 b4
    \grace { f16 a16 } b2
    \afterGrace b2 { f16[ a16] }
    \appoggiatura a32 b4
    \acciaccatura a8 b4
  }
  \new Lyrics
  \lyricsto melody {
    normal
    \set includeGraceNotes = ##t
    case,
    gra -- ce case,
    after -- grace case,
    \set ignoreMelismata = ##t
    app. case,
    acc. case.
  }
>>

Bekannte Probleme und Warnungen
Wie bei associatedVoice muss includeGraceNotes spätestens eine Silbe vor derjenigen gesetzt werden, die unter einer Verzierungsnote stehen soll. Im Fall, dass eine Verzierungsnote die erste des Musikstückes ist, kann ein \with- oder \context-Block verwendet werden:

<<
  \new Voice = melody \relative c' {
    \grace { c16( d e f }
    g1) f
  }
  \new Lyrics \with { includeGraceNotes = ##t }
  \lyricsto melody {
    Ah __ fa
  }
>>

Die Strophen am Ende ausdrucken
Manchmal soll nur eine Strophe mit der Melodie gesetzt werden und die weiteren Strophen als Text unter den Noten hinzugefügt werden. Dazu wird der Text in einer markup-Umgebung außerhalb der \score-Umgebung gesetzt. Es gibt zwei Arten, die Zeilen auszurichten, wie das Beispiel zeigt:

melody = \relative {
e' d c d | e e e e |
d d e d | c1 |
}

text = \lyricmode {
\set stanza = "1." Ma- ry had a lit- tle lamb,
its fleece was white as snow.
}

\score{ <<
  \new Voice = "one" { \melody }
  \new Lyrics \lyricsto "one" \text
>>
  \layout { }
}
\markup { \column{
  \line{ Verse 2. }
  \line{ All the children laughed and played }
  \line{ To see a lamb at school. }
  }
}
\markup{
  \wordwrap-string "
  Verse 3.

  Mary took it home again,

  It was against the rule."
}

Die Strophen am Ende in mehreren Spalten drucken
Wenn in einem Lied sehr viele Strophen vorkommen, werden sie oft in mehreren Spalten unter den Noten gesetzt. Eine nach außen versetzte Zahl zeigt die Strophennummer an. Dieses Beispiel zeigt eine Methode, diese Art von Notensatz zu produzieren.

melody = \relative {
  c'4 c c c | d d d d
}

text = \lyricmode {
  \set stanza = "1." This is verse one.
  It has two lines.
}

\score{ <<
    \new Voice = "one" { \melody }
    \new Lyrics \lyricsto "one" \text
   >>
  \layout { }
}

\markup {
  \fill-line {
    \hspace #0.1 % moves the column off the left margin;
     % can be removed if space on the page is tight
     \column {
      \line { \bold "2."
        \column {
          "This is verse two."
          "It has two lines."
        }
      }
      \combine \null \vspace #0.1 % adds vertical spacing between verses
      \line { \bold "3."
        \column {
          "This is verse three."
          "It has two lines."
        }
      }
    }
    \hspace #0.1 % adds horizontal spacing between columns;
    \column {
      \line { \bold "4."
        \column {
          "This is verse four."
          "It has two lines."
        }
      }
      \combine \null \vspace #0.1 % adds vertical spacing between verses
      \line { \bold "5."
        \column {
          "This is verse five."
          "It has two lines."
        }
      }
    }
  \hspace #0.1 % gives some extra space on the right margin;
  % can be removed if page space is tight
  }
}
*/
