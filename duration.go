package lilypond

import (
	"fmt"
	"strings"
)

type Duration struct {
	dur  int
	dots uint
}

func (d Duration) String() string {
	var dur string

	switch {
	case d.dur > 0:
		dur = fmt.Sprintf("%v", d.dur)
	case d.dur == -8:
		dur = `\maxima`
	case d.dur == -4:
		dur = `\longa`
	case d.dur == -2:
		dur = `\breve`
	case d.dur == 0:
		// do nothing
	default:
		panic(fmt.Sprintf("invalid duration time: %v", d.dur))
	}

	if d.dots > 0 {
		dur = dur + strings.Repeat(".", int(d.dots))
	}
	return dur
}

func newDuration(dur int, dots uint) Duration {
	return Duration{
		dur:  dur,
		dots: dots,
	}
}

func Longa() Duration                { return newDuration(-4, 0) }
func Maxima() Duration               { return newDuration(-8, 0) }
func Breve() Duration                { return newDuration(-2, 0) }
func Whole() Duration                { return newDuration(1, 0) }
func Half() Duration                 { return newDuration(2, 0) }
func Quarter() Duration              { return newDuration(4, 0) }
func Eightth() Duration              { return newDuration(8, 0) }
func Sixteenth() Duration            { return newDuration(16, 0) }
func ThirtySecond() Duration         { return newDuration(32, 0) }
func SixtyFourth() Duration          { return newDuration(64, 0) }
func HundredTwentyEightth() Duration { return newDuration(128, 0) }

func LongaDot(num uint) Duration                { return newDuration(-4, num) }
func MaximaDot(num uint) Duration               { return newDuration(-8, num) }
func BreveDot(num uint) Duration                { return newDuration(-2, num) }
func WholeDot(num uint) Duration                { return newDuration(1, num) }
func HalfDot(num uint) Duration                 { return newDuration(2, num) }
func QuarterDot(num uint) Duration              { return newDuration(4, num) }
func EightthDot(num uint) Duration              { return newDuration(8, num) }
func SixteenthDot(num uint) Duration            { return newDuration(16, num) }
func ThirtySecondDot(num uint) Duration         { return newDuration(32, num) }
func SixtyFourthDot(num uint) Duration          { return newDuration(64, num) }
func HundredTwentyEightthDot(num uint) Duration { return newDuration(128, num) }
