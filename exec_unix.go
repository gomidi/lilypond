// +build !windows

package lilypond

import (
	"os/exec"
	"strings"
)

func _execCommand(c string) *exec.Cmd {
	return exec.Command("/bin/sh", "-c", "exec "+c)
}

func cmdMidi2Ly(file string, args []string) *exec.Cmd {
	var s strings.Builder

	s.WriteString("midi2ly")

	for _, arg := range args {
		s.WriteString(" " + arg)
	}

	s.WriteString(" " + file)

	//fmt.Println("running " + s.String())

	return _execCommand(s.String())
}

func cmdLilypond(file string, args []string) *exec.Cmd {
	var s strings.Builder

	s.WriteString("lilypond")

	for _, arg := range args {
		s.WriteString(" " + arg)
	}

	s.WriteString(" " + file)

	//fmt.Println("running " + s.String())
	return _execCommand(s.String())
}

/*
func midiCatOutCmd(index int) *exec.Cmd {
	cmd := exec.Command("midicat", "out", fmt.Sprintf("--index=%v", index))
	//	cmd.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}
	return cmd
}

func midiCatInCmd(index int) *exec.Cmd {
	cmd := exec.Command("midicat", "in", fmt.Sprintf("--index=%v", index))
	//	cmd.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}
	return cmd
}

func midiCatCmd(args string) *exec.Cmd {
	cmd := _execCommand("midicat " + args)
	// important! prevents that signals such as interrupt send to the main program gets passed
	// to midicat (which would not allow us to shutdown properly, e.g. stop hanging notes)
	//cmd.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}
	return cmd
}
*/
