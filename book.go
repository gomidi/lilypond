package lilypond

import (
	"bytes"
	"fmt"
	"os"

	"gitlab.com/golang-utils/fs/filesystems/rootfs"
	"gitlab.com/golang-utils/fs/path"
)

type Book struct {
	Score  *Score
	Header *Header
}

// this is interesting:
// https://lilypond.org/doc/v2.18/Documentation/snippets/spacing
// \markup \vspace #1 %avoid LSR-bug

func (s Book) String() string {
	var bf bytes.Buffer

	bf.WriteString(`\book { ` + "\n")
	bf.WriteString(s.Score.String())
	bf.WriteString(s.Header.String())
	bf.WriteString("\n } \n")

	return bf.String()
}

func (s Book) ToPDF(pdffile path.Local) error {

	fsys, err := rootfs.New()

	if err != nil {
		return err
	}

	tdirloc := path.MustDirFromSystem(os.TempDir())

	var all string

	all = fmt.Sprintf(`\version %q`+"\n", "2.24.4")
	all += s.String()

	lyfile, err := fsys.WriteTmpFile(tdirloc.RootRelative(), "temp-*.ly", []byte(all))

	if err != nil {
		return err
	}

	defer fsys.Delete(lyfile, false)

	var c Command
	c.LilypondFile = path.ToSystem(fsys.Abs(lyfile))
	c.Output = path.ToSystem(pdffile)
	c.Formats = "pdf"
	c.Silent = true
	return c.Run()
}

func NewBook(s Staffer) *Book {
	b := &Book{
		Score:  newScore(),
		Header: &Header{},
	}
	b.Score.staffer = s
	return b
}
