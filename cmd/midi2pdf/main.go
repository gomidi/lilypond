package main

import (
	"fmt"
	"os"

	"gitlab.com/golang-utils/config/v2"
	"gitlab.com/gomidi/lilypond"
)

var (
	cfg           = config.New("midi2pdf", "converts a midi file to a PDF score via lilypond")
	argInputFile  = cfg.LastString("midifile", "The SMF/MIDI file that should be converted into a score.", config.Required())
	argOutputfile = cfg.String("out", "the output PDF file that should be written to (without pdf file extension).", config.Shortflag('o'))
	argDebug      = cfg.Bool("debug", "show debuging informations")
)

func main() {
	err := run()

	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %s\n", err.Error())
		os.Exit(1)
	}
}

func run() error {

	err := cfg.Run()

	if err != nil {
		return err
	}

	return lilypond.MIDI2PDF(argInputFile.Get(), argOutputfile.Get(), argDebug.Get())
}
