package lilypond

import "bytes"

type Score struct {
	staffer        Staffer
	Layout         *Layout
	Header         *Header
	pageBreakAfter bool
}

func (s *Score) SetPageBreakAfter() *Score {
	s.pageBreakAfter = true
	return s
}

func (s Score) String() string {
	var bf bytes.Buffer

	bf.WriteString(`\score { ` + "\n")

	if s.staffer != nil {
		bf.WriteString(s.staffer.String())
	}

	bf.WriteString("\n } \n")

	bf.WriteString(s.Layout.String())
	bf.WriteString(s.Header.String())

	if s.pageBreakAfter {
		bf.WriteString(`\pageBreak` + "\n")
	}

	return bf.String()
}

func newScore() *Score {
	return &Score{
		Layout: &Layout{},
		Header: &Header{},
	}
}
