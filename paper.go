package lilypond

import "bytes"

type Paper struct {
	elements []Element
}

func (l *Header) SetPrintAllHeaders() *Header {
	l.Add(Line("print-all-headers = ##t"))
	return l
}

func (l *Paper) Add(elms ...Element) *Paper {
	l.elements = append(l.elements, elms...)
	return l
}

func (l Paper) String() string {
	if len(l.elements) == 0 {
		return ""
	}
	var bf bytes.Buffer

	bf.WriteString(`\paper { ` + "\n")

	for _, elm := range l.elements {
		bf.WriteString(elm.String())
	}

	bf.WriteString("\n } \n")

	return bf.String()
}
