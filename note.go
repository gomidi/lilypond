package lilypond

import "strings"

type Mode string

const RelaiveMode Mode = `\relative`

type noAutobeam struct{}

func (n noAutobeam) String() string {
	return `\autoBeamOff` + "\n"
}

var AutoBeamOff = noAutobeam{}

const ChangeStaffUp = Line(`\change Staff = up`)
const ChangeStaffDown = Line(`\change Staff = down`)

type Note struct {
	note        string
	octave      int
	duration    Duration
	startSlur   bool
	endSlur     bool
	startPhrase bool
	endPhrase   bool
	startBeam   bool
	endBeam     bool
	suffix      string
	elements    []Element
}

/*
Sprache      Kreuz     B       Doppelkreuz   Doppel-B
nederlands   is        es      isis          eses
*/

func (n *Note) Is() *Note {
	n.suffix = "is"
	return n
}

func (n *Note) Es() *Note {
	n.suffix = "es"
	return n
}

func (n *Note) Isis() *Note {
	n.suffix = "isis"
	return n
}

func (n *Note) Eses() *Note {
	n.suffix = "eses"
	return n
}

/*
Sprache      Vierteltonkreuz    Viertelton-B   3/4-Tonkreuz   3/4-Ton-B
nederlands   ih                 eh             isih           eseh
*/

func (n *Note) Ih() *Note {
	n.suffix = "ih"
	return n
}

func (n *Note) Eh() *Note {
	n.suffix = "eh"
	return n
}

func (n *Note) Isih() *Note {
	n.suffix = "isih"
	return n
}

func (n *Note) Eseh() *Note {
	n.suffix = "eseh"
	return n
}

func (n Note) String() string {
	dur := n.duration.String()
	var res = n.note + n.suffix

	switch {
	case n.octave < 0:
		res = res + strings.Repeat(`,`, n.octave*(-1)) + dur
	case n.octave > 0:
		res = res + strings.Repeat(`'`, n.octave) + dur
	default:
		res = res + dur
	}

	if n.startPhrase {
		res = res + `\(`
	}

	if n.startSlur {
		res = res + "("
	}

	if n.endSlur {
		res = res + ")"
	}

	if n.startPhrase {
		res = res + `\)`
	}

	if n.startBeam {
		res = res + `[`
	}

	if n.endBeam {
		res = res + `]`
	}

	for _, elm := range n.elements {
		res = res + elm.String()
	}

	return res + " "
}

/*
\ppppp, \pppp, \ppp, \pp, \p, \mp, \mf, \f, \ff, \fff, \ffff, fffff, \fp, \sf, \sff, \sp, \spp, \sfz, and \rfz
*/

/*

https://lilypond.org/doc/v2.24/Documentation/notation/expressive-marks-attached-to-notes

Dynamik
Absolute Dynamikbezeichnung wird mit Befehlen nach den Noten angezeigt, etwa c4\ff. Die vordefinierten Befehle lauten: \ppppp, \pppp, \ppp, \pp, \p, \mp, \mf, \f, \ff, \fff, \ffff, fffff, \fp, \sf, \sff, \sp, \spp, \sfz, and \rfz. Die Dynamikzeichen können manuell unter- oder oberhalb des Systems platziert werden, siehe Richtung und Platzierung.

\relative c'' {
  c2\ppp c\mp
  c2\rfz c^\mf
  c2_\spp c^\ff
}

Eine Crescendo-Klammer wird mit dem Befehl \< begonnen und mit \!, einem absoluten Dynamikbefehl oder einer weiteren Crescendo- oder Decrescendo-Klammer beendet. Ein Decrescendo beginnt mit \> und wird auch beendet mit \!, einem absoluten Dynamikbefehl oder einem weiteren Crescendo oder Decrescendo. \cr und \decr können anstelle von \< und \> benutzt werden. Die Befehle ergeben standardmäßig Crescendo-Klammern.

\relative c'' {
  c2\< c\!
  d2\< d\f
  e2\< e\>
  f2\> f\!
  e2\> e\mp
  d2\> d\>
  c1\!
}

Eine Crescendo-Klammer, die mit \! beendet wird, endet an der rechten Seite der Note, welcher \! zugeordnet ist. In dem Fall, dass es durch den Beginn eines anderen crescendo- oder decrescendo-Zeichens beendet wird, endet es in der Mitte der Note, welche das nächste \< oder \> angehängt hat. Die nächste Klammer beginnt dann am rechten Rand der selben Note anstatt dem normalerweise linken Rand, wenn die vorherige Klammer mit \! beendet worden wäre.

Der \espressivo-Befehl kann eingesetzt werden, um crescendo und decrescendo für die selbe Note anzuzeigen. Dieser Befehl ist jedoch als Artikulation, nicht als Dynamikzeichen implementiert.

\relative {
  c''2 b4 a
  g1\espressivo
}

Mit Text gesetzte Crescendo-Bezeichnungen beginnen mit \cresc. Mit Text gesetzte Decrescendo-Bezeichnungen beginnen mit \decresc oder \dim. Fortsetzungslinien werden gesetzt, wenn sie benötigt werden.

\relative {
  g'8\cresc a b c b c d e\mf |
  f8\decresc e d c e\> d c b |
  a1\dim ~ |
  a2. r4\! |
}

*/

func (n *Note) PPPPP() *Note {
	n.Add(String(`\ppppp`))
	return n
}

func (n *Note) PPPP() *Note {
	n.Add(String(`\pppp`))
	return n
}

func (n *Note) PPP() *Note {
	n.Add(String(`\ppp`))
	return n
}

func (n *Note) PP() *Note {
	n.Add(String(`\pp`))
	return n
}

func (n *Note) P() *Note {
	n.Add(String(`\p`))
	return n
}

func (n *Note) MP() *Note {
	n.Add(String(`\mp`))
	return n
}

func (n *Note) MF() *Note {
	n.Add(String(`\mf`))
	return n
}

func (n *Note) F() *Note {
	n.Add(String(`\f`))
	return n
}

func (n *Note) FF() *Note {
	n.Add(String(`\ff`))
	return n
}

func (n *Note) FFF() *Note {
	n.Add(String(`\fff`))
	return n
}

func (n *Note) FFFF() *Note {
	n.Add(String(`\ffff`))
	return n
}

func (n *Note) FFFFF() *Note {
	n.Add(String(`\fffff`))
	return n
}

func (n *Note) FP() *Note {
	n.Add(String(`\fp`))
	return n
}

func (n *Note) SF() *Note {
	n.Add(String(`\sf`))
	return n
}

func (n *Note) SFF() *Note {
	n.Add(String(`\sff`))
	return n
}

func (n *Note) SP() *Note {
	n.Add(String(`\sp`))
	return n
}

func (n *Note) SPP() *Note {
	n.Add(String(`\spp`))
	return n
}

func (n *Note) SFZ() *Note {
	n.Add(String(`\sfz`))
	return n
}

func (n *Note) RFZ() *Note {
	n.Add(String(`\rfz`))
	return n
}

func (n *Note) EndDyn() *Note {
	n.Add(String(`\!`))
	return n
}

func (n *Note) Cresc() *Note {
	n.Add(String(`\cresc`))
	return n
}

func (n *Note) Decresc() *Note {
	n.Add(String(`\decresc `))
	return n
}

func (n *Note) Dim() *Note {
	n.Add(String(`\dim`))
	return n
}

/*
\relative c'' {
  aeses4 aes ais a
  \set Staff.extraNatural = ##f
  aeses4 aes ais a
}

Sprache				Notenbezeichnungen
nederlands    c d e f g a bes b

Sprache      Kreuz     B       Doppelkreuz   Doppel-B
nederlands   is        es      isis          eses

Sprache      Vierteltonkreuz    Viertelton-B   3/4-Tonkreuz   3/4-Ton-B
nederlands   ih                 eh             isih           eseh
*/

// \slurDown
// \slurUp
// \slurNeutral

/*
Artikulationszeichen und Verzierungen
Eine Vielfalt an Symbolen kann über und unter den Noten erscheinen, um zu markieren, auf welche Art die Note ausgeführt werden soll. Hierzu wird folgende Syntax benutzt:

Note\Bezeichnung
Die möglichen Werte für Bezeichnung sind aufgelistet in Liste der Artikulationszeichen. Ein Beispiel:

\relative {
  c''4\staccato c\mordent b2\turn
  c1\fermata
}

https://lilypond.org/doc/v2.24/Documentation/notation/list-of-articulations

Artikulationsskripte
	\accent
	\espressivo
	\marcato or -^
	\portato or -_
	\staccatissimo or -!
	\staccato or -.
	\tenuto or --

Ornamentale Skripte
	\prall
	\prallup
	\pralldown
	\upprall
	\downprall
	\prallprall
	\lineprall
	\prallmordent
	\mordent
	\upmordent
	\downmordent
	\trill
	\turn
	\reverseturn
	\slashturn
	\haydnturn

Fermatenskripte
	\veryshortfermata
	\shortfermata
	\fermata
	\longfermata
	\verylongfermata
	\henzeshortfermata
	\henzelongfermata

Instrumentenspezifische Skripte
	\upbow
	\downbow
	\flageolet
	\open
	\halfopen
	\lheel
	\rheel
	\ltoe
	\rtoe
	\snappizzicato
	\stopped or -+

Wiederholungszeichenskripte
	\segno
	\coda
	\varcoda

*/

func (n *Note) NoBeam() *Note {
	n.Add(String(`\noBeam`))
	return n
}

func (n *Note) Accent() *Note {
	n.Add(String(`\accent`))
	return n
}
func (n *Note) Espressivo() *Note {
	n.Add(String(`\espressivo`))
	return n
}
func (n *Note) Marcato() *Note {
	n.Add(String(`\marcato`))
	return n
}
func (n *Note) Portato() *Note {
	n.Add(String(`\portato`))
	return n
}
func (n *Note) Staccatissimo() *Note {
	n.Add(String(`\staccatissimo`))
	return n
}
func (n *Note) Staccato() *Note {
	n.Add(String(`\staccato`))
	return n
}
func (n *Note) Tenuto() *Note {
	n.Add(String(`\tenuto`))
	return n
}
func (n *Note) Prall() *Note {
	n.Add(String(`\prall`))
	return n
}
func (n *Note) Prallup() *Note {
	n.Add(String(`\prallup`))
	return n
}
func (n *Note) Pralldown() *Note {
	n.Add(String(`\pralldown`))
	return n
}
func (n *Note) Upprall() *Note {
	n.Add(String(`\upprall`))
	return n
}
func (n *Note) Downprall() *Note {
	n.Add(String(`\downprall`))
	return n
}
func (n *Note) Prallprall() *Note {
	n.Add(String(`\prallprall`))
	return n
}
func (n *Note) Lineprall() *Note {
	n.Add(String(`\lineprall`))
	return n
}
func (n *Note) Prallmordent() *Note {
	n.Add(String(`\prallmordent`))
	return n
}
func (n *Note) Mordent() *Note {
	n.Add(String(`\mordent`))
	return n
}
func (n *Note) Upmordent() *Note {
	n.Add(String(`\upmordent`))
	return n
}
func (n *Note) Downmordent() *Note {
	n.Add(String(`\downmordent`))
	return n
}
func (n *Note) Trill() *Note {
	n.Add(String(`\trill`))
	return n
}
func (n *Note) Turn() *Note {
	n.Add(String(`\turn`))
	return n
}
func (n *Note) Reverseturn() *Note {
	n.Add(String(`\reverseturn`))
	return n
}
func (n *Note) Slashturn() *Note {
	n.Add(String(`\slashturn`))
	return n
}
func (n *Note) Haydnturn() *Note {
	n.Add(String(`\haydnturn`))
	return n
}
func (n *Note) Veryshortfermata() *Note {
	n.Add(String(`\veryshortfermata`))
	return n
}
func (n *Note) Shortfermata() *Note {
	n.Add(String(`\shortfermata`))
	return n
}
func (n *Note) Fermata() *Note {
	n.Add(String(`\fermata`))
	return n
}
func (n *Note) Longfermata() *Note {
	n.Add(String(`\longfermata`))
	return n
}
func (n *Note) Verylongfermata() *Note {
	n.Add(String(`\verylongfermata`))
	return n
}
func (n *Note) Henzeshortfermata() *Note {
	n.Add(String(`\henzeshortfermata`))
	return n
}
func (n *Note) Henzelongfermata() *Note {
	n.Add(String(`\henzelongfermata`))
	return n
}
func (n *Note) Upbow() *Note {
	n.Add(String(`\upbow`))
	return n
}
func (n *Note) Downbow() *Note {
	n.Add(String(`\downbow`))
	return n
}
func (n *Note) Flageolet() *Note {
	n.Add(String(`\flageolet`))
	return n
}
func (n *Note) Open() *Note {
	n.Add(String(`\open`))
	return n
}
func (n *Note) Halfopen() *Note {
	n.Add(String(`\halfopen`))
	return n
}
func (n *Note) Lheel() *Note {
	n.Add(String(`\lheel`))
	return n
}
func (n *Note) Rheel() *Note {
	n.Add(String(`\rheel`))
	return n
}
func (n *Note) Ltoe() *Note {
	n.Add(String(`\ltoe`))
	return n
}
func (n *Note) Rtoe() *Note {
	n.Add(String(`\rtoe`))
	return n
}
func (n *Note) Snappizzicato() *Note {
	n.Add(String(`\snappizzicato`))
	return n
}
func (n *Note) Stopped() *Note {
	n.Add(String(`\stopped`))
	return n
}
func (n *Note) Coda() *Note {
	n.Add(String(`\coda`))
	return n
}
func (n *Note) Varcoda() *Note {
	n.Add(String(`\varcoda`))
	return n
}

func (n *Note) Segno() *Note {
	n.Add(String(`\segno`))
	return n
}

func (n *Note) Glissando() *Note {
	n.Add(String(`\glissando`))
	return n
}

func (n *Note) StartSlur() *Note {
	n.startSlur = true
	return n
}

func (n *Note) EndSlur() *Note {
	n.endSlur = true
	return n
}

func (n *Note) StartPhrase() *Note {
	n.startPhrase = true
	return n
}

func (n *Note) EndPhrase() *Note {
	n.endPhrase = true
	return n
}

func (n *Note) StartBeam() *Note {
	n.startBeam = true
	return n
}

func (n *Note) EndBeam() *Note {
	n.endBeam = true
	return n
}

func (n *Note) Add(elms ...Element) *Note {
	n.elements = append(n.elements, elms...)
	return n
}

func (n *Note) SetDuration(dur int) *Note {
	n.duration = newDuration(dur, 0)
	return n
}

func (n *Note) SetDurationDot(dur int, dots uint) *Note {
	n.duration = newDuration(dur, dots)
	return n
}

func (n *Note) Maxima() *Note {
	return n.SetDuration(-8)
}

func (n *Note) Longa() *Note {
	return n.SetDuration(-4)
}

func (n *Note) Breve() *Note {
	return n.SetDuration(-2)
}

func (n *Note) Whole() *Note {
	return n.SetDuration(1)
}

func (n *Note) Half() *Note {
	return n.SetDuration(2)
}

func (n *Note) Quarter() *Note {
	return n.SetDuration(4)
}

func (n *Note) Eightth() *Note {
	return n.SetDuration(8)
}

func (n *Note) Sixteenth() *Note {
	return n.SetDuration(16)
}

func (n *Note) ThirtySecond() *Note {
	return n.SetDuration(32)
}

func (n *Note) SixtyFourth() *Note {
	return n.SetDuration(64)
}

func (n *Note) HundredTwentyEightth() *Note {
	return n.SetDuration(128)
}

func (n *Note) MaximaDot(num uint) *Note {
	return n.SetDurationDot(-8, num)
}

func (n *Note) LongaDot(num uint) *Note {
	return n.SetDurationDot(-4, num)
}

func (n *Note) BreveDot(num uint) *Note {
	return n.SetDurationDot(-2, num)
}

func (n *Note) WholeDot(num uint) *Note {
	return n.SetDurationDot(1, num)
}

func (n *Note) HalfDot(num uint) *Note {
	return n.SetDurationDot(2, num)
}

func (n *Note) QuarterDot(num uint) *Note {
	return n.SetDurationDot(4, num)
}

func (n *Note) EightthDot(num uint) *Note {
	return n.SetDurationDot(8, num)
}

func (n *Note) SixteenthDot(num uint) *Note {
	return n.SetDurationDot(16, num)
}

func (n *Note) ThirtySecondDot(num uint) *Note {
	return n.SetDurationDot(32, num)
}

func (n *Note) SixtyFourthDot(num uint) *Note {
	return n.SetDurationDot(64, num)
}

func (n *Note) HundredTwentyEightthDot(num uint) *Note {
	return n.SetDurationDot(128, num)
}

func A(octave int) *Note {
	return &Note{
		note:   "a",
		octave: octave,
	}
}

func B(octave int) *Note {
	return &Note{
		note:   "b",
		octave: octave,
	}
}

func C(octave int) *Note {
	return &Note{
		note:   "c",
		octave: octave,
	}
}

func D(octave int) *Note {
	return &Note{
		note:   "d",
		octave: octave,
	}
}

func E(octave int) *Note {
	return &Note{
		note:   "e",
		octave: octave,
	}
}

func F(octave int) *Note {
	return &Note{
		note:   "f",
		octave: octave,
	}
}

func G(octave int) *Note {
	return &Note{
		note:   "g",
		octave: octave,
	}
}

/*
cymc cyms cymr hh hhc hho hhho hhp
  cb hc bd sn ss tomh tommh tomml toml tomfh tomfl
*/

/*
			bda1^\markup { \center-align "acousticbassdrum: bda" }
      bd  _\markup { \center-align "bassdrum: bd" }
      sn  ^\markup { \center-align "snare: sn" }
      sne _\markup { \center-align "electricsnare: sne" }
      sna ^\markup { \center-align "acousticsnare: sna" } \myBreak

      tomfl^\markup { \center-align "lowfloortom: tomfl" }
      tomfh_\markup { \center-align "highfloortom: tomfh" }
      toml ^\markup { \center-align "lowtom: toml" }
      tomh _\markup { \center-align "hightom: tomh" }
      tomml^\markup { \center-align "lowmidtom: tomml" }
      tommh_\markup { \center-align "himidtom: tommh" } \myBreak

      hhc ^\markup { \center-align "closedhihat: hhc" }
      hh  _\markup { \center-align "hihat: hh" }
      hhp ^\markup { \center-align "pedalhihat: hhp" }
      hho _\markup { \center-align "openhihat: hho" }
      hhho^\markup { \center-align "halfopenhihat: hhho" } \myBreak

      cymca^\markup { \center-align "crashcymbala: cymca" }
      cymc _\markup { \center-align "crashcymbal: cymc" }
      cymra^\markup { \center-align "ridecymbala: cymra" }
      cymr _\markup { \center-align "ridecymbal: cymr" } \myBreak

      cymch^\markup { \center-align "chinesecymbal: cymch" }
      cyms _\markup { \center-align "splashcymbal: cyms" }
      cymcb^\markup { \center-align "crashcymbalb: cymcb" }
      cymrb_\markup { \center-align "ridecymbalb: cymrb" }
      rb   ^\markup { \center-align "ridebell: rb" }
      cb   _\markup { \center-align "cowbell: cb" } \myBreak

      bohm^\markup { \center-align "mutehibongo: bohm" }
      boh _\markup { \center-align "hibongo: boh" }
      boho^\markup { \center-align "openhibongo: boho" }
      bolm_\markup { \center-align "mutelobongo: bolm" }
      bol ^\markup { \center-align "lobongo: bol" }
      bolo_\markup { \center-align "openlobongo: bolo" } \myBreak

      cghm^\markup { \center-align "mutehiconga: cghm" }
      cglm_\markup { \center-align "muteloconga: cglm" }
      cgho^\markup { \center-align "openhiconga: cgho" }
      cgh _\markup { \center-align "hiconga: cgh" }
      cglo^\markup { \center-align "openloconga: cglo" }
      cgl _\markup { \center-align "loconga: cgl" } \myBreak

      timh^\markup { \center-align "hitimbale: timh" }
      timl_\markup { \center-align "lotimbale: timl" }
      agh ^\markup { \center-align "hiagogo: agh" }
      agl _\markup { \center-align "loagogo: agl" } \myBreak

      ssh^\markup { \center-align "hisidestick: ssh" }
      ss _\markup { \center-align "sidestick: ss" }
      ssl^\markup { \center-align "losidestick: ssl" } \myBreak

      guis^\markup { \center-align "shortguiro: guis" }
      guil_\markup { \center-align "longguiro: guil" }
      gui ^\markup { \center-align "guiro: gui" }
      cab _\markup { \center-align "cabasa: cab" }
      mar ^\markup { \center-align "maracas: mar" } \myBreak

      whs^\markup { \center-align "shortwhistle: whs" }
      whl_\markup { \center-align "longwhistle: whl" } \myBreak

      hc  ^\markup { \center-align "handclap: hc" }
      tamb_\markup { \center-align "tambourine: tamb" }
      vibs^\markup { \center-align "vibraslap: vibs" }
      tt  _\markup { \center-align "tamtam: tt"  } \myBreak

      cl ^\markup { \center-align "claves: cl" }
      wbh_\markup { \center-align "hiwoodblock: wbh" }
      wbl^\markup { \center-align "lowoodblock: wbl" } \myBreak

      cuim^\markup { \center-align "mutecuica: cuim" }
      cuio_\markup { \center-align "opencuica: cuio" }
      trim^\markup { \center-align "mutetriangle: trim" }
      tri _\markup { \center-align "triangle: tri" }
      trio^\markup { \center-align "opentriangle: trio" } \myBreak
*/

func Acousticbassdrum() *Note {
	return &Note{
		note:   "acousticbassdrum",
		octave: 0,
	}
}
func Bassdrum() *Note {
	return &Note{
		note:   "bassdrum",
		octave: 0,
	}
}
func Snare() *Note {
	return &Note{
		note:   "snare",
		octave: 0,
	}
}
func Electricsnare() *Note {
	return &Note{
		note:   "electricsnare",
		octave: 0,
	}
}
func Acousticsnare() *Note {
	return &Note{
		note:   "acousticsnare",
		octave: 0,
	}
}
func Lowfloortom() *Note {
	return &Note{
		note:   "lowfloortom",
		octave: 0,
	}
}
func Highfloortom() *Note {
	return &Note{
		note:   "highfloortom",
		octave: 0,
	}
}
func Lowtom() *Note {
	return &Note{
		note:   "lowtom",
		octave: 0,
	}
}
func Hightom() *Note {
	return &Note{
		note:   "hightom",
		octave: 0,
	}
}
func Lowmidtom() *Note {
	return &Note{
		note:   "lowmidtom",
		octave: 0,
	}
}
func Himidtom() *Note {
	return &Note{
		note:   "himidtom",
		octave: 0,
	}
}
func Closedhihat() *Note {
	return &Note{
		note:   "closedhihat",
		octave: 0,
	}
}
func Hihat() *Note {
	return &Note{
		note:   "hihat",
		octave: 0,
	}
}
func Pedalhihat() *Note {
	return &Note{
		note:   "pedalhihat",
		octave: 0,
	}
}
func Openhihat() *Note {
	return &Note{
		note:   "openhihat",
		octave: 0,
	}
}
func Halfopenhihat() *Note {
	return &Note{
		note:   "halfopenhihat",
		octave: 0,
	}
}
func Crashcymbala() *Note {
	return &Note{
		note:   "crashcymbala",
		octave: 0,
	}
}
func Crashcymbal() *Note {
	return &Note{
		note:   "crashcymbal",
		octave: 0,
	}
}
func Ridecymbala() *Note {
	return &Note{
		note:   "ridecymbala",
		octave: 0,
	}
}
func Ridecymbal() *Note {
	return &Note{
		note:   "ridecymbal",
		octave: 0,
	}
}
func Chinesecymbal() *Note {
	return &Note{
		note:   "chinesecymbal",
		octave: 0,
	}
}
func Splashcymbal() *Note {
	return &Note{
		note:   "splashcymbal",
		octave: 0,
	}
}
func Crashcymbalb() *Note {
	return &Note{
		note:   "crashcymbalb",
		octave: 0,
	}
}
func Ridecymbalb() *Note {
	return &Note{
		note:   "ridecymbalb",
		octave: 0,
	}
}
func Ridebell() *Note {
	return &Note{
		note:   "ridebell",
		octave: 0,
	}
}
func Cowbell() *Note {
	return &Note{
		note:   "cowbell",
		octave: 0,
	}
}
func Mutehibongo() *Note {
	return &Note{
		note:   "mutehibongo",
		octave: 0,
	}
}
func Hibongo() *Note {
	return &Note{
		note:   "hibongo",
		octave: 0,
	}
}
func Openhibongo() *Note {
	return &Note{
		note:   "openhibongo",
		octave: 0,
	}
}
func Mutelobongo() *Note {
	return &Note{
		note:   "mutelobongo",
		octave: 0,
	}
}
func Lobongo() *Note {
	return &Note{
		note:   "lobongo",
		octave: 0,
	}
}
func Openlobongo() *Note {
	return &Note{
		note:   "openlobongo",
		octave: 0,
	}
}
func Mutehiconga() *Note {
	return &Note{
		note:   "mutehiconga",
		octave: 0,
	}
}
func Muteloconga() *Note {
	return &Note{
		note:   "muteloconga",
		octave: 0,
	}
}
func Openhiconga() *Note {
	return &Note{
		note:   "openhiconga",
		octave: 0,
	}
}
func Hiconga() *Note {
	return &Note{
		note:   "hiconga",
		octave: 0,
	}
}
func Openloconga() *Note {
	return &Note{
		note:   "openloconga",
		octave: 0,
	}
}
func Loconga() *Note {
	return &Note{
		note:   "loconga",
		octave: 0,
	}
}
func Hitimbale() *Note {
	return &Note{
		note:   "hitimbale",
		octave: 0,
	}
}
func Lotimbale() *Note {
	return &Note{
		note:   "lotimbale",
		octave: 0,
	}
}
func Hiagogo() *Note {
	return &Note{
		note:   "hiagogo",
		octave: 0,
	}
}
func Loagogo() *Note {
	return &Note{
		note:   "loagogo",
		octave: 0,
	}
}
func Hisidestick() *Note {
	return &Note{
		note:   "hisidestick",
		octave: 0,
	}
}
func Sidestick() *Note {
	return &Note{
		note:   "sidestick",
		octave: 0,
	}
}
func Losidestick() *Note {
	return &Note{
		note:   "losidestick",
		octave: 0,
	}
}
func Shortguiro() *Note {
	return &Note{
		note:   "shortguiro",
		octave: 0,
	}
}
func Longguiro() *Note {
	return &Note{
		note:   "longguiro",
		octave: 0,
	}
}
func Guiro() *Note {
	return &Note{
		note:   "guiro",
		octave: 0,
	}
}
func Cabasa() *Note {
	return &Note{
		note:   "cabasa",
		octave: 0,
	}
}
func Maracas() *Note {
	return &Note{
		note:   "maracas",
		octave: 0,
	}
}
func Shortwhistle() *Note {
	return &Note{
		note:   "shortwhistle",
		octave: 0,
	}
}
func Longwhistle() *Note {
	return &Note{
		note:   "longwhistle",
		octave: 0,
	}
}
func Handclap() *Note {
	return &Note{
		note:   "handclap",
		octave: 0,
	}
}
func Tambourine() *Note {
	return &Note{
		note:   "tambourine",
		octave: 0,
	}
}
func Vibraslap() *Note {
	return &Note{
		note:   "vibraslap",
		octave: 0,
	}
}
func Tamtam() *Note {
	return &Note{
		note:   "tamtam",
		octave: 0,
	}
}
func Claves() *Note {
	return &Note{
		note:   "claves",
		octave: 0,
	}
}
func Hiwoodblock() *Note {
	return &Note{
		note:   "hiwoodblock",
		octave: 0,
	}
}
func Lowoodblock() *Note {
	return &Note{
		note:   "lowoodblock",
		octave: 0,
	}
}
func Mutecuica() *Note {
	return &Note{
		note:   "mutecuica",
		octave: 0,
	}
}
func Opencuica() *Note {
	return &Note{
		note:   "opencuica",
		octave: 0,
	}
}
func Mutetriangle() *Note {
	return &Note{
		note:   "mutetriangle",
		octave: 0,
	}
}
func Triangle() *Note {
	return &Note{
		note:   "triangle",
		octave: 0,
	}
}
func Opentriangle() *Note {
	return &Note{
		note:   "opentriangle",
		octave: 0,
	}
}

func Rest() *Note {
	return &Note{
		note:   "r",
		octave: 0,
	}
}

func Silent() *Note {
	return &Note{
		note:   "s",
		octave: 0,
	}
}
