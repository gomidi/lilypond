package lilypond

import "bytes"

type Chord struct {
	notes    []*Note
	duration *Duration
}

func (c *Chord) Add(notes ...*Note) *Chord {
	c.notes = append(c.notes, notes...)
	return c
}

func (c *Chord) SetDuration(dur Duration) *Chord {
	c.duration = &dur
	return c
}

func (c *Chord) String() string {
	if len(c.notes) == 0 {
		return ""
	}

	var bf bytes.Buffer

	bf.WriteString("<")

	for _, n := range c.notes {
		bf.WriteString(n.String() + " ")
	}

	bf.WriteString(">")

	if c.duration != nil {
		bf.WriteString(c.duration.String())
	}

	bf.WriteString(" ")

	return bf.String()
}

/*
Noten mit Akkorden
Ein Akkord wird notiert, indem die zu ihm gehörenden Tonhöhen zwischen spitze Klammern (< und >) gesetzt werden. Auf einen Akkord kann eine Dauer-Angabe folgen, genauso wie bei einfachen Noten.

\relative {
  <a' c e>1 <a c e>2 <f a c e>4 <a c>8. <g c e>16
}

Akkorde können auch von Artikulationen gefolgt werden, genau wie auch einfache Noten.

\relative {
  <a' c e>1\fermata <a c e>2-> <f a c e>4\prall <a c>8.^! <g c e>16-.
}

Die Noten innerhalb der Akkorde konnen auch von Artikulationen oder Ornamenten gefolgt werden.

\relative {
  <a' c\prall e>1 <a-> c-^ e>2 <f-. a c-. e-.>4
  <a-+ c-->8. <g\fermata c e\turn>16
}

Manche Notationselemente, wie etwa Dynamik, Crescendo-Klammern und Legatobögen müssen an den gesamten Akkord gehängt werden und nicht an einzelne Noten, damit sie ausgegeben werden.

\relative {
  <a'\f c( e>1 <a c) e>\f <a\< c e>( <a\! c e>)
  <a c e>\< <a c e> <a c e>\!
}

*/

/*
https://lilypond.org/doc/v2.24/Documentation/notation/displaying-rhythms#time-signature

Akkordnotation für Gitarren bezeichnet auch oft zusätzlich den geschlagenen Rhythmus. Das kann notiert werden unter Verwendung des Pitch_squash_engraver und indem Tonhöhenimprovisation eingeschaltet wird mit \improvisationOn.

<<
  \new ChordNames {
    \chordmode {
      c1 f g c
    }
  }

  \new Voice \with {
    \consists Pitch_squash_engraver
  } \relative c'' {
    \improvisationOn
    c4 c8 c c4 c8 c
    f4 f8 f f4 f8 f
    g4 g8 g g4 g8 g
    c4 c8 c c4 c8 c
  }
>>

Schlagrhythmus für Gitarren
In Gitarrennotation kann neben Melodie, Akkordbezeichnungen und Bunddiagrammen auch der Schlagrhythmus angegeben werden.

\include "predefined-guitar-fretboards.ly"
<<
  \new ChordNames {
    \chordmode {
      c1 | f | g | c
    }
  }
  \new FretBoards {
    \chordmode {
      c1 | f | g | c
    }
  }
  \new Voice \with {
    \consists "Pitch_squash_engraver"
  } {
    \relative c'' {
      \improvisationOn
      c4 c8 c c4 c8 c
      f4 f8 f f4 f8 f
      g4 g8 g g4 g8 g
      c4 c8 c c4 c8 c
    }
  }
  \new Voice = "melody" {
    \relative c'' {
      c2 e4 e4
      f2. r4
      g2. a4
      e4 c2.
    }
  }
  \new Lyrics {
    \lyricsto "melody" {
      This is my song.
      I like to sing.
    }
  }
>>

*/
