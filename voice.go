package lilypond

import (
	"bytes"
	"fmt"
)

type Voice struct {
	name          string
	isDrumvoice   bool
	elements      []Element
	with          []Element
	lyrics        *Lyrics
	voiceName     string
	stemDirection int // 0 = auto > 0 = up < 0 = down
}

func (l *Voice) SetDrumVoice() *Voice {
	l.isDrumvoice = true
	return l
}

func (l *Voice) StemDirectionUp() *Voice {
	l.stemDirection = 1
	return l
}

func (l *Voice) StemDirectionDown() *Voice {
	l.stemDirection = -1
	return l
}

func (l *Voice) SetName(name string) *Voice {
	l.name = name
	return l
}

func (v *Voice) With(s ...Element) *Voice {
	v.with = append(v.with, s...)
	return v
}

func (v *Voice) Add(s ...Element) *Voice {
	v.elements = append(v.elements, s...)
	return v
}

func (v *Voice) NewLyrics() *Lyrics {
	v.lyrics = &Lyrics{}
	v.lyrics.lyricsTo = v.name
	return v.lyrics
}

func (v Voice) HasLyrics() bool {
	if v.lyrics == nil {
		return false
	}
	return len(v.lyrics.elements) > 0
}

func (v Voice) String() string {
	var bf bytes.Buffer

	tname := "Voice"

	if v.isDrumvoice {
		tname = "DrumVoice"
	}

	if v.name != "" {
		fmt.Fprintf(&bf, `\new %s = %q`, tname, v.name)
	} else {
		fmt.Fprintf(&bf, `\new %s`, tname)
	}

	if len(v.with) > 0 {
		fmt.Fprintf(&bf, ` \with { `+"\n")

		for _, w := range v.with {
			bf.WriteString(w.String())
		}

		bf.WriteString("\n } \n")
	}

	bf.WriteString(" { \n")

	if v.voiceName != "" {
		bf.WriteString(`\` + v.voiceName + " \n")
	}

	switch {
	case v.stemDirection == 0:
	case v.stemDirection > 0:
		bf.WriteString(`\override Stem.direction = #UP ` + "\n")
	case v.stemDirection < 0:
		bf.WriteString(`\override Stem.direction = #DOWN ` + "\n")
	}

	for _, s := range v.elements {
		bf.WriteString(s.String())
	}

	bf.WriteString("\n } \n")

	if v.HasLyrics() {
		bf.WriteString(v.lyrics.String())
	}

	return bf.String()
}
