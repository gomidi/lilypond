package lilypond

import (
	"strings"
	"testing"

	"gitlab.com/golang-utils/fs/path"
)

func TestChoir(t *testing.T) {

	// the example from here:
	// https://lilypond.org/doc/v2.23/Documentation/learning/four_002dpart-satb-vocal-score
	// transscribed without relative
	var (
		global   = ElementGroup(KeyMajor(D(0)), TimeSignature{4, 4})
		sopranos = new(Staff).SetName("sopranos").With(InstrumentName("Soprano"))
		altos    = new(Staff).SetName("altos").With(InstrumentName("Alto"))
		tenors   = new(Staff).SetName("tenors").With(InstrumentName("Tenor"))
		basses   = new(Staff).SetName("basses").With(InstrumentName("Bass"))
		lh       = new(Staff).SetName("lh")
		rh       = new(Staff).SetName("rh")

		ch = new(ChoirStaff).Add(sopranos, altos, tenors, basses)
		ps = new(PianoStaff).Set(lh, rh).With(InstrumentName("Piano"))
		g  = new(StaffGroup).Add(ch, ps).Loose() // Loose prevents the "StaffGroup" string from being printed
	)

	words := ElementGroup(
		String("Wor -- thy"), BarChange,
		String("is the lamb"), BarChange,
		String("that was slain"), BarChange,
	)

	sopranos.NewVoice().SetName("sopranos").Add(ClefTreble(0), global,
		Rest().Quarter(), D(2).Half(), A(1).Quarter(), BarChange,
		D(2).QuarterDot(1), D(2).Eightth(), A(1).Half(), BarChange,
		C(2).Is().Quarter(), D(2), C(2).Is().Half(), BarChange,
	).NewLyrics().Add(words)

	altos.NewVoice().SetName("altos").Add(ClefTreble(0), global,
		Rest().Quarter(), A(1).Half(), A(1).Quarter(), BarChange,
		F(1).Is().QuarterDot(1), F(1).Is().Eightth(), A(1).Half(), BarChange,
		G(1).Quarter(), F(1).Is(), F(1).Is().Half(), BarChange,
	).NewLyrics().Add(words)

	tenors.NewVoice().SetName("tenors").Add(Clef_G(8), global,
		Rest().Quarter(), F(1).Is().Half(), E(1).Quarter(), BarChange,
		D(1).QuarterDot(1), D(1).Eightth(), D(1).Half(), BarChange,
		E(1).Quarter(), A(0), C(1).Is().Half(), BarChange,
	).NewLyrics().Add(words)

	basses.NewVoice().SetName("basses").Add(ClefBass(0), global,
		Rest().Quarter(), D(1).Half(), C(1).Is().Quarter(), BarChange,
		B(0).QuarterDot(1), B(0).Eightth(), F(0).Is().Half(), BarChange,
		E(0).Quarter(), D(0), A(0).Half(), BarChange,
	).NewLyrics().Add(words)

	lh.NewVoice().Add(ClefTreble(0), global,
		Rest().Quarter(),
		new(Chord).Add(A(1), D(2), F(2).Is()).SetDuration(Half()),
		new(Chord).Add(A(1), E(2), A(2)).SetDuration(Quarter()),
		BarChange,
		new(Chord).Add(D(3), F(2).Is(), D(2)).SetDuration(QuarterDot(1)),
		new(Chord).Add(D(3), F(2).Is(), D(2)).SetDuration(Eightth()),
		new(Chord).Add(A(1), D(2), A(2)).SetDuration(Half()),
		BarChange,
		new(Chord).Add(G(1), C(2).Is(), G(2)).SetDuration(Quarter()),
		new(Chord).Add(A(1), D(2), F(2).Is()).SetDuration(Quarter()),
		new(Chord).Add(A(1), C(2).Is(), E(2)).SetDuration(Half()),
		BarChange,
	)

	rh.NewVoice().Add(ClefBass(0), global,
		new(Chord).Add(D(0), D(-1)).SetDuration(Quarter()),
		new(Chord).Add(D(0), D(-1)).SetDuration(Half()),
		new(Chord).Add(C(0).Is(), C(-1).Is()).SetDuration(Quarter()),
		BarChange,
		new(Chord).Add(B(0), B(-1)).SetDuration(QuarterDot(1)),
		new(Chord).Add(B(-2), B(-1)).SetDuration(Eightth()),
		new(Chord).Add(F(0).Is(), F(-1).Is()).SetDuration(Half()),
		BarChange,
		new(Chord).Add(E(0), E(-1)).SetDuration(Quarter()),
		new(Chord).Add(D(0), D(-1)).SetDuration(Quarter()),
		new(Chord).Add(A(0), A(-1)).SetDuration(Half()),
		BarChange,
	)

	bk := NewBook(g)

	//	fmt.Println(bk.String())

	err := bk.ToPDF(path.MustWD().Join("TestChoir"))

	if err != nil {
		t.Errorf("error while producing pdf: %s", err.Error())
	}
}

func TestSimple1(t *testing.T) {
	var (
		st1 = new(Staff)
		bk  = NewBook(st1)
	)

	st1.NewVoice().Add(A(1), B(2))

	expected := strings.TrimSpace(`
\book { 
\score { 
\new Staff << 
\new Voice { 
a' b'' 
 } 

 >> 

 } 

 }
`)

	got := strings.TrimSpace(bk.String())

	if got != expected {
		t.Errorf("got: \n%q\n\nexpected: \n%q\n", got, expected)
	}

	err := bk.ToPDF(path.MustWD().Join("TestSimple1"))

	if err != nil {
		t.Errorf("error while producing pdf: %s", err.Error())
	}
}

func TestSimple2(t *testing.T) {
	var (
		st1 = new(Staff).SetName("mystaff")
		bk  = NewBook(st1)
	)

	st1.NewVoice().Add(A(1), B(2)).SetName("myvoice")

	expected := strings.TrimSpace(`
\book { 
\score { 
\new Staff = "mystaff" << 
\new Voice = "myvoice" { 
a' b'' 
 } 

 >> 

 } 

 }
`)

	got := strings.TrimSpace(bk.String())

	if got != expected {
		t.Errorf("got: \n%q\n\nexpected: \n%q\n", got, expected)
	}

	err := bk.ToPDF(path.MustWD().Join("TestSimple2"))

	if err != nil {
		t.Errorf("error while producing pdf: %s", err.Error())
	}
}

func TestSimpleMultiVoice(t *testing.T) {
	var (
		st1 = new(Staff).SetName("mystaff")
		bk  = NewBook(st1)
	)

	st1.NewVoice().SetName("myvoice1").Add(A(1), B(2))
	st1.NewVoice().SetName("myvoice2").Add(C(1), D(2))

	expected := strings.TrimSpace(`
\book { 
\score { 
\new Staff = "mystaff" << 
\new Voice = "myvoice1" { 
\voiceOne 
a' b'' 
 } 
\new Voice = "myvoice2" { 
\voiceTwo 
c' d'' 
 } 

 >> 

 } 

 }
`)

	got := strings.TrimSpace(bk.String())

	if got != expected {
		t.Errorf("got: \n%q\n\nexpected: \n%q\n", got, expected)
	}

	err := bk.ToPDF(path.MustWD().Join("TestSimpleMultiVoice"))

	if err != nil {
		t.Errorf("error while producing pdf: %s", err.Error())
	}
}

func TestSimpleStaffGroup(t *testing.T) {
	var (
		st1 = new(Staff).SetName("mystaff1")
		st2 = new(Staff).SetName("mystaff2")
		g   StaffGroup
		bk  = NewBook(g.Add(st1, st2))
	)

	st1.NewVoice().SetName("myvoice1").Add(A(1), B(2))
	st2.NewVoice().SetName("myvoice2").Add(C(1), D(2))

	expected := strings.TrimSpace(`
\book { 
\score { 
\new StaffGroup << 
\new Staff = "mystaff1" << 
\new Voice = "myvoice1" { 
a' b'' 
 } 

 >> 
\new Staff = "mystaff2" << 
\new Voice = "myvoice2" { 
c' d'' 
 } 

 >> 

 >> 

 } 

 }
`)

	got := strings.TrimSpace(bk.String())

	if got != expected {
		t.Errorf("got: \n%q\n\nexpected: \n%q\n", got, expected)
	}

	err := bk.ToPDF(path.MustWD().Join("TestSimpleStaffGroup"))

	if err != nil {
		t.Errorf("error while producing pdf: %s", err.Error())
	}
}

func TestSimpleStaffGroupMultiVoice(t *testing.T) {
	var (
		st1 = new(Staff).SetName("mystaff1")
		st2 = new(Staff).SetName("mystaff2")
		g   StaffGroup
		bk  = NewBook(g.Add(st1, st2))
	)

	st1.NewVoice().SetName("myvoice1").Add(A(1), B(2))
	st1.NewVoice().SetName("myvoice2").Add(C(1), D(2))
	st2.NewVoice().SetName("myvoice3").Add(E(1), F(2))
	st2.NewVoice().SetName("myvoice4").Add(G(1), A(2))

	//sc1.Staffer .Add(st1, st2)

	expected := strings.TrimSpace(`
\book { 
\score { 
\new StaffGroup << 
\new Staff = "mystaff1" << 
\new Voice = "myvoice1" { 
\voiceOne 
a' b'' 
 } 
\new Voice = "myvoice2" { 
\voiceTwo 
c' d'' 
 } 

 >> 
\new Staff = "mystaff2" << 
\new Voice = "myvoice3" { 
\voiceOne 
e' f'' 
 } 
\new Voice = "myvoice4" { 
\voiceTwo 
g' a'' 
 } 

 >> 

 >> 

 } 

 }
`)

	got := strings.TrimSpace(bk.String())

	if got != expected {
		t.Errorf("got: \n%q\n\nexpected: \n%q\n", got, expected)
	}

	err := bk.ToPDF(path.MustWD().Join("TestSimpleStaffGroupMultiVoice"))

	if err != nil {
		t.Errorf("error while producing pdf: %s", err.Error())
	}
}

func TestGrandStaff(t *testing.T) {
	var (
		st1 = new(Staff).SetName("mystaff1")
		st2 = new(Staff).SetName("mystaff2")
		st3 = new(Staff).SetName("mystaff3")
		st4 = new(Staff).SetName("mystaff4")
		g1  StaffGroup
		g2  StaffGroup
		gr  GrandStaff
		bk  = NewBook(gr.Add(g1.Add(st1, st2), g2.Add(st3, st4)))
	)

	st1.NewVoice().SetName("myvoice1").Add(A(1), B(2))
	st2.NewVoice().SetName("myvoice2").Add(C(1), D(2))
	st3.NewVoice().SetName("myvoice3").Add(E(1), F(2))
	st4.NewVoice().SetName("myvoice4").Add(G(1), A(2))

	expected := strings.TrimSpace(`
\book { 
\score { 
\new GrandStaff << 
\new StaffGroup << 
\new Staff = "mystaff1" << 
\new Voice = "myvoice1" { 
a' b'' 
 } 

 >> 
\new Staff = "mystaff2" << 
\new Voice = "myvoice2" { 
c' d'' 
 } 

 >> 

 >> 
\new StaffGroup << 
\new Staff = "mystaff3" << 
\new Voice = "myvoice3" { 
e' f'' 
 } 

 >> 
\new Staff = "mystaff4" << 
\new Voice = "myvoice4" { 
g' a'' 
 } 

 >> 

 >> 

 >> 

 } 

 }
`)

	got := strings.TrimSpace(bk.String())

	if got != expected {
		t.Errorf("got: \n%q\n\nexpected: \n%q\n", got, expected)
	}

	err := bk.ToPDF(path.MustWD().Join("TestGrandStaff"))

	if err != nil {
		t.Errorf("error while producing pdf: %s", err.Error())
	}
}

func TestPianoStaff(t *testing.T) {
	var (
		lh = new(Staff).SetName("left hand")
		rh = new(Staff).SetName("right hand")
		ps = new(PianoStaff).Set(lh, rh)
		bk = NewBook(ps)
	)

	lh.NewVoice().SetName("myvoice1").Add(A(1).Eightth(), B(2))
	lh.NewVoice().SetName("myvoice2").Add(C(1), D(2))
	lh.NewVoice().SetName("myvoice3").Add(E(1), F(2))
	rh.NewVoice().SetName("myvoice4").Add(ClefBass(0), G(-1), A(-2))

	expected := strings.TrimSpace(`
\book { 
\score { 
\new PianoStaff << 
\new Staff = "left hand" << 
\new Voice = "myvoice1" { 
\voiceOne 
a'8 b'' 
 } 
\new Voice = "myvoice2" { 
\voiceTwo 
c' d'' 
 } 
\new Voice = "myvoice3" { 
\voiceThree 
e' f'' 
 } 

 >> 
\new Staff = "right hand" << 
\new Voice = "myvoice4" { 
\clef "bass"
g, a,, 
 } 

 >> 

 >> 

 } 

 }
`)

	got := strings.TrimSpace(bk.String())

	if got != expected {
		t.Errorf("got: \n%q\n\nexpected: \n%q\n", got, expected)
	}

	err := bk.ToPDF(path.MustWD().Join("TestPianoStaff"))

	if err != nil {
		t.Errorf("error while producing pdf: %s", err.Error())
	}
}
