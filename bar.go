package lilypond

import "fmt"

// Duration is the time until the next bar
type BarPartial Duration

// maybe better alternative (since partial can only be used at the beginning):
// \set Timing.measurePosition = #(ly:make-moment -1/4)
// see https://lilypond.org/doc/v2.24/Documentation/notation/displaying-rhythms#time-signature

func (p BarPartial) String() string {
	// \partial 8
	return fmt.Sprintf(`\partial %v`+"\n", Duration(p).String())
}

type TempoName string

func (t TempoName) String() string {
	return fmt.Sprintf(`\tempo %q`+"\n", string(t))
}

type Metronome struct {
	Name     string
	Duration Duration
	BPM      int
}

func (m Metronome) String() string {
	nm := ""
	if m.Name != "" {
		nm = fmt.Sprintf(`%q `, m.Name)
	}
	return fmt.Sprintf(`\tempo %s%s = %v`+"\n", nm, m.Duration.String(), m.BPM)
}

// \breathe

// \phrasingSlurUp
// \phrasingSlurDown
// \phrasingSlurNeutral

type barChange struct{}

func (b barChange) String() string {
	return " | \n"
}

type endbar struct{}

func (b endbar) String() string {
	return `\bar "|." ` + "\n"
}

/*
Manuell können zwei einfache Taktstriche und zusätzlich fünf Arten eines doppelten Taktstriches gesetzt werden:

\relative {
  f'1 \bar "|"
  f1 \bar "."
  g1 \bar "||"
  a1 \bar ".|"
  b1 \bar ".."
  c1 \bar "|.|"
  d1 \bar "|."
  e1
}
[image of music]

Zusätzlich gibt es noch punktierte und gestrichelte Taktstriche:

\relative {
  f'1 \bar ";"
  g1 \bar "!"
  a1
}

und fünf unterschiedliche Wiederholungstaktstriche:

f1 \bar ".|:" g \bar ":..:" a \bar ":|.|:" b \bar ":|.:" c \bar ":|." d
*/

/*
\set Score.currentBarNumber = #50
*/

// \bar "|."

var BarChange = barChange{}

// \compressEmptyMeasures
// \expandEmptyMeasures
// \numericTimeSignature
// \defaultTimeSignature

type BarRest struct {
	TimeSignature [2]uint
	Duration      uint
}

func (r BarRest) String() string {
	// no rest
	if r.Duration == 0 {
		return ""
	}

	// R1*duration
	if r.TimeSignature[0] == 0 {
		return fmt.Sprintf("R1*%v", r.Duration)
	}

	res := fmt.Sprintf("R%v", r.Duration)

	// Rduration*length
	if r.TimeSignature[1] == 0 {
		return res + fmt.Sprintf("*%v", r.TimeSignature[0])
	}

	// Rduration*num/denom
	return res + fmt.Sprintf("*%v/%v", r.TimeSignature[0], r.TimeSignature[1])
}

type TimeSignature [2]uint

func (b TimeSignature) String() string {
	return fmt.Sprintf(`\time %v/%v`+"\n", b[0], b[1])
}

/*
\clef french
c2 c
\clef soprano
c2 c
\clef mezzosoprano
c2 c
\clef baritone
c2 c

\break

\clef varbaritone
c2 c
\clef subbass
c2 c
\clef percussion
c2 c

\break

\clef G   % synonym for treble
c2 c
\clef F   % synonym for bass
c2 c
\clef C   % synonym for alto
c2 c

Indem _8 oder ^8 an die jeweilige Schlüsselbezeichnung angehängt wird, wird der Schlüssel um eine Oktave nach oben oder unten transponiert, mit _15 oder ^15 um zwei Oktaven.
Auch andere Ganzzahlen können verwendet werden, wenn es gewünscht wird.
Die Schlüsselbezeichnung muss in Anführungszeichen gesetzt werden,
wenn nicht-alphabetische Zeichen enthält, siehe Beispiel:

\clef treble
c'2 c'
\clef "treble_8"
c'2 c'
\clef "bass^15"
c'2 c'
\clef "alto_2"
c'2 c'
\clef "G_8"
c'2 c'
\clef "F^5"
c'2 c'
*/

type clef string

func (c clef) String() string {
	return fmt.Sprintf(`\clef %q`, string(c)) + "\n"
}

func clefN(s string, n int) clef {
	c := s
	switch {
	case n < 0:
		c += fmt.Sprintf("_%v", n*(-1))
	case n > 0:
		c += fmt.Sprintf("^%v", n)
	default:
		// do nothing
	}

	return clef(c)
}

// transpose might be 8 (octave up), -8 (octave down), 15 (two octaves up), -15 (two octaves down) or any number
// 0 = no transposition
func ClefTreble(transpose int) clef {
	return clefN("treble", transpose)
}

func ClefFrench(transpose int) clef {
	return clefN("french", transpose)
}

func ClefSoprano(transpose int) clef {
	return clefN("soprano", transpose)
}

func ClefMezzoSoprano(transpose int) clef {
	return clefN("mezzosoprano", transpose)
}

func ClefAlto(transpose int) clef {
	return clefN("alto", transpose)
}

func ClefTenor(transpose int) clef {
	return clefN("tenor", transpose)
}

func ClefBass(transpose int) clef {
	return clefN("bass", transpose)
}

func ClefBaritone(transpose int) clef {
	return clefN("baritone", transpose)
}

func ClefVarBaritone(transpose int) clef {
	return clefN("varbaritone", transpose)
}

func ClefSubBass(transpose int) clef {
	return clefN("subbass", transpose)
}

func ClefPercussion(transpose int) clef {
	return clefN("percussion", transpose)
}

func Clef_C(transpose int) clef {
	return clefN("C", transpose)
}

func Clef_G(transpose int) clef {
	return clefN("G", transpose)
}

func Clef_F(transpose int) clef {
	return clefN("F", transpose)
}

type key string

func mkKey(note *Note, name string) key {
	return key(fmt.Sprintf(`%s \%s`, note.note+note.suffix, name))
}

func (c key) String() string {
	return `\key ` + string(c) + "\n"
}

func KeyMajor(note *Note) key {
	return mkKey(note, "major")
}

func KeyMinor(note *Note) key {
	return mkKey(note, "minor")
}

func KeyIonian(note *Note) key {
	return mkKey(note, "ionian")
}

func KeyLocrian(note *Note) key {
	return mkKey(note, "locrian")
}

func KeyAeolian(note *Note) key {
	return mkKey(note, "aeolian")
}

func KeyMixolydian(note *Note) key {
	return mkKey(note, "mixolydian")
}

func KeyLydian(note *Note) key {
	return mkKey(note, "lydian")
}

func KeyPhrygian(note *Note) key {
	return mkKey(note, "phrygian")
}

func KeyDorian(note *Note) key {
	return mkKey(note, "dorian")
}
