package lilypond

/*
Gleichzeitige Ausdrücke
Eine oder mehrere musikalische Ausdrücke, die in doppelte spitze Klammern eingeschlossen werden, werden gleichzeitig gesetzt. Wenn der erste Ausdruck mit einer einzelnen Note beginnt oder die gesamte Konstruktion explizit in einer einzelnen Stimme erstellt wird, wird auch nur ein Notensystem erstellt. In anderem Falle werden die Elemente der simultanen Konstruktion auf unterschiedlichen Systemen gesetzt.

Das nächste Beispiel zeigt simultane Konstruktionen auf einem System:

\new Voice {  % explicit single voice
  << \relative { a'4 b g2 }
     \relative { d'4 g c,2 } >>
}

Vorübergehende polyphone Passagen

Ein vorübergehender polyphoner Abschnitt kann mit folgender Konstruktion erstellt werden:

<< { \voiceOne ... }
  \new Voice { \voiceTwo ... }
>> \oneVoice
Der erste Ausdruck innerhalb des polyphonen Abschnitts wird in den Voice-Kontext gestellt, der unmittelbar vor dem polyphonen Abschnitt aktiv war, und der gleiche Voice-Kontext setzt sich nach dem Abschnitt fort. Andere Ausdrücke innerhalb der eckigen Klammern werden anderen Stimmennummern zugewiesen. Damit lassen sich auch Gesangstexte einer durchgehenden Stimme vor, während und nach dem polyphonen Abschnitt zuweisen:

\relative <<
  \new Voice = "melody" {
    a'4
    <<
      {
        \voiceOne
        g f
      }
      \new Voice {
        \voiceTwo
        d2
      }
    >>
    \oneVoice
    e4
  }
  \new Lyrics \lyricsto "melody" {
  This is my song.
  }
>>

Die Konstruktion mit doppeltem Backslash

Die << {...} \\ {...} >>-Konstruktion, in welcher die beiden (oder mehreren) Ausdrücke durch doppelte Backslash-Zeichen (Taste AltGr+ß) getrennt werden, verhält sich anderes als die ähnliche Konstruktion ohne die doppelten Schrägstriche: alle Ausdrücke innerhalb der eckigen Klammern werden in diesem Fall jeweils neuen Voice-Kontexten zugeordnet. Diese neuen Voice-Kontexte werden implizit erstellt und haben die festen Bezeichnungen "1", "2" usw.

Das erste Beispiel könnte also auch wie folgt notiert werden:

<<
  \relative { r8 r16 g'' e8. f16 g8[ c,] f e16 d }
  \\
  \relative { d''16 c d8~ 16 b c8~ 16 b c8~ 16 b8. }
>>
[image of music]

Diese Syntax kann benutzt werden, wenn es keine Rolle spielt, ob vorübergehend Stimmen erstellt werden und dann wieder verworfen werden. Diese implizit erstellten Stimmen erhalten die Einstellungen, die in den Befehlen \voiceOne … \voiceFour enthalten sind, in der Reihenfolge, in der sie im Quelltext auftauchen.

Im nächsten Beispiel zeigen die Hälse der zeitweiligen Stimme nach oben, sie wird deshalb erst als dritte in der Konstruktion notiert, damit sie die Eigenschaften von voiceThree zugewiesen bekommt. Unsichtbare Pause werden eingesetzt, damit keine doppelten Pausen ausgegeben werden.

<<
  \relative { r8 g'' g  g g f16 ees f8 d }
  \\
  \relative { ees'8 r ees r d r d r }
  \\
  \relative { d''8 s c s bes s a s }
>>

Stimmen-Anordnung

Wenn mehrere Stimmen notiert werden, sollte folgende Anordnung eingehalten werden:

Stimme 1: höchste
Stimme 2: tiefste
Stimme 3: zweithöchste
Stimme 4: zweittiefste
Stimme 5: dritthöchste
Stimme 6: dritttiefste
usw.
Auch wenn das erst nicht einleuchtend erscheint, erleichtert es den automatischen Layoutprozess doch sehr. Die ungeraden Stimmen erhalten Hälse nach oben, die graden Stimmen Hälse nach unten:

\new Staff <<
  \time 2/4
  { f''2 }  % 1: highest
  \\
  { c'2  }  % 2: lowest
  \\
  { d''2 }  % 3: second-highest
  \\
  { e'2  }  % 4: second-lowest
  \\
  { b'2  }  % 5: third-highest
  \\
  { g'2  }  % 6: third-lowest
>>
*/

/*
https://lilypond.org/doc/v2.24/Documentation/notation/multiple-voices

Mehrstimmigkeit in einem System
Stimmen explicit beginnen

Die grundlegende Struktur, die man benötigt, um mehrere unabhängige Stimmen in einem Notensystem zu setzen, ist im Beispiel unten dargestellt:

\new Staff <<
  \new Voice = "first"
    \relative { \voiceOne r8 r16 g'' e8. f16 g8[ c,] f e16 d }
  \new Voice= "second"
    \relative { \voiceTwo d''16 c d8~ 16 b c8~ 16 b c8~ 16 b8. }
>>

\relative <<
  \new Voice = "melody" {
    a'4
    <<
      {
        \voiceOne
        g f
      }
      \new Voice {
        \voiceTwo
        d2
      }
    >>
    \oneVoice
    e4
  }
  \new Lyrics \lyricsto "melody" {
  This is my song.
  }
>>

instrumentOne = \relative {
  c'4 d e f |
  R1 |
  d'4 c b a |
  b4 g2 f4 |
  e1 |
}

instrumentTwo = \relative {
  R1 |
  g'4 a b c |
  d4 c b a |
  g4 f( e) d |
  e1 |
}

<<
  \new Staff \instrumentOne
  \new Staff \instrumentTwo
>>

Musik parallel notieren
Noten für mehrere Stimmen können verschachtelt notiert werden. Die Funktion \parallelMusic akzeptiert eine Liste mit den Bezeichnungen einer Reihe von Variablen und einen musikalischen Ausdruck. Der Inhalt der verschiedenen Takte in dem musikalischen Ausdruck bekommt die Bezeichnung der Variablen zugewiesen, sodass sie benutzt werden können, um die Musik dann zu setzen. Dabei entspricht jede Zeile einer Stimme.

\parallelMusic voiceA,voiceB,voiceC {
  % Bar 1
  r8 g'16 c'' e'' g' c'' e'' r8 g'16 c'' e'' g' c'' e'' |
  r16 e'8.~   4              r16 e'8.~   4              |
  c'2                        c'2                        |

  % Bar 2
  r8 a'16 d'' f'' a' d'' f'' r8 a'16 d'' f'' a' d'' f'' |
  r16 d'8.~   4              r16 d'8.~   4              |
  c'2                        c'2                        |

}
\new StaffGroup <<
  \new Staff << \voiceA \\ \voiceB >>
  \new Staff { \clef bass \voiceC }
>>
*/

/*
3.4.1 Stücke durch Variablen organisieren
Wenn alle die Elemente, die angesprochen wurden, zu größeren Dateien zusammengefügt werden, werden auch die musikalischen Ausdrücke sehr viel größer. In polyphonen Dateien mit vielen Systemen kann das sehr chaotisch aussehen. Das Chaos kann aber deutlich reduziert werden, wenn Variablen definiert und verwendet werden.

Variablen (die auch als Bezeichner oder Makros bezeichnet werden) können einen Teil der Musik aufnehmen. Sie werden wie folgt definiert:

bezeichneteMusik = { … }

In den Namen der Variablen dürfen nur Buchstaben des Alphabets verwendet werden, keine Zahlen oder Striche.

Variable müssen vor dem eigentlichen musikalischen Ausdruck definiert werden. Sie können dann aber beliebig oft verwendet werden, nachdem sie einmal definiert worden sind. Sie können sogar eingesetzt werden, um später in der Datei eine neue Variable zu erstellen. Damit kann die Schreibarbeit erleichtert werden, wenn Notengruppen sich oft wiederholen.

Der Inhalt des musikalischen Ausdrucks bezeichneteMusik kann dann später wieder benutzt werden, indem man einen Backslash davor setzt (\bezeichneteMusik), genau wie bei jedem LilyPond-Befehl.

violin = \new Staff {
  \relative {
    a'4 b c b
  }
}

cello = \new Staff {
  \relative {
    \clef "bass"
    e2 d
  }
}

{
  <<
    \violin
    \cello
  >>
}

Man kann diese Variablen auch für viele andere Objekte verwenden, etwa:

myWidth = 60      % eine Zahl für die \paper variable
                  % (die Einheit ist Millimeter)
myName = "Wendy"  % eine Zeichenkette für eine Textbeschriftung
aFivePaper = \paper { #(set-paper-size "a5") }
Abhängig vom Kontext kann solch ein Bezeichner in verschiedenen Stellen verwendet werden. Das folgende Beispiel zeigt die Benutzung der eben definierten Bezeichner:

\paper {
  \aFivePaper
  line-width = \myWidth
}

{
  c4^\myName
}

*/
