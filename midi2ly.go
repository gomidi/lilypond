package lilypond

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

/*
type Midi2LyOption func(*Midi2Ly)
*/

type Midi2Ly struct {
	Version bool
	Help    bool
	Debug   bool
	Quiet   bool
	Verbose bool

	Preview           bool // 4 first bars only
	AbsolutePitches   bool
	ExplicitDurations bool
	Skip              bool
	TextLyrics        bool // treat text as lyrics

	StartQuant    int // z.B. 32tel
	DurationQuant int // z.B. 32 für 32tel

	Key           string   // +Kreuze|-B's: MOLL=1 z.B. -2:1
	IncludeHeader string   // include file
	AllowTuplet   []string // DAUER*ZÄHLER/NENNER z.B. 4*2/3

	Output   string // output file
	MIDIFile string // input file
}

func FlatMinorKey(flats uint8) string {
	return fmt.Sprintf("-%v:1", flats)
}

func SharpMinorKey(sharps uint8) string {
	return fmt.Sprintf("%v:1", sharps)
}

func FlatMajorKey(flats uint8) string {
	return fmt.Sprintf("-%v", flats)
}

func SharpMajorKey(sharps uint8) string {
	return fmt.Sprintf("%v", sharps)
}

func AllowedTuplet(duration, numerator, denominator uint8) string {
	return fmt.Sprintf("%v*%v/%v", duration, numerator, denominator)
}

type arg struct {
	key   string
	value interface{}
}

func (a arg) String() string {
	switch a.value.(type) {
	case string:
		//	return fmt.Sprintf("%s='%s'", a.key, a.value)
		return fmt.Sprintf("%v=%v", a.key, a.value)
	case bool:
		return a.key
	default:
		return fmt.Sprintf("%v=%v", a.key, a.value)
	}
}

func removeFileExtension(filename string) string {
	idx := strings.LastIndex(filename, ".")
	return filename[:idx]
}

type args []arg

func (a args) Strings() (s []string) {
	for _, ar := range a {
		s = append(s, ar.String())
	}

	return
}

func MIDI2PDF(midifile, pdffile string, debug bool) error {
	var ml Midi2Ly
	if debug {
		ml.Debug = true
	} else {
		ml.Quiet = true
	}

	ml.MIDIFile = midifile
	return ml.ToPDF(pdffile)
}

func (m Midi2Ly) ToPDF(pdffile string) error {
	if m.MIDIFile == "" {
		return fmt.Errorf("MIDIFile not set")
	}

	if m.Output == "" {
		m.Output = m.MIDIFile + ".ly"
	}

	err := m.Run()

	if err != nil {
		return fmt.Errorf("could not convert midi file to lilypond file: %s", err.Error())
	}

	lyfile, err := ioutil.ReadFile(m.Output)

	if err != nil {
		return err
	}

	content := string(lyfile)

	content = strings.ReplaceAll(content, `\midi {}`, "")

	err = ioutil.WriteFile(m.Output, []byte(content), 0644)

	if err != nil {
		return err
	}

	var ly Command
	ly.LilypondFile = m.Output
	if m.Debug || m.Verbose {
		ly.Verbose = true
	}
	if m.Quiet {
		ly.Silent = true
	}
	ly.Output = pdffile
	if ly.Output == "" {
		ly.Output = removeFileExtension(m.Output)
	}
	return ly.Run()
}

func (m Midi2Ly) Run() error {
	cmd := cmdMidi2Ly(m.MIDIFile, m.Args().Strings())
	//return cmd.Run()
	out, err := cmd.CombinedOutput()
	fmt.Fprintf(os.Stdout, string(out))

	return err
}

func (m Midi2Ly) Args() (a args) {

	if m.Version {
		a = append(a, arg{"--version", true})
	}

	if m.Help {
		a = append(a, arg{"--help", true})
	}

	if m.Debug {
		a = append(a, arg{"--debug", true})
	}

	if m.Quiet {
		a = append(a, arg{"--quiet", true})
	}

	if m.Verbose {
		a = append(a, arg{"--verbose", true})
	}

	if m.Preview {
		a = append(a, arg{"--preview", true})
	}

	if m.AbsolutePitches {
		a = append(a, arg{"--absolute-pitches", true})
	}

	if m.ExplicitDurations {
		a = append(a, arg{"--explicit-durations", true})
	}

	if m.Skip {
		a = append(a, arg{"--skip", true})
	}

	if m.TextLyrics {
		a = append(a, arg{"--text-lyrics", true})
	}

	if m.StartQuant != 0 {
		a = append(a, arg{"--start-quant", m.StartQuant})
	}

	if m.DurationQuant != 0 {
		a = append(a, arg{"--duration-quant", m.DurationQuant})
	}

	if m.Key != "" {
		a = append(a, arg{"--key", m.Key})
	}

	if m.IncludeHeader != "" {
		a = append(a, arg{"--include-header", m.IncludeHeader})
	}

	for _, at := range m.AllowTuplet {
		if at != "" {
			a = append(a, arg{"--allow-tuplet", m.AllowTuplet})
		}
	}

	if m.Output != "" {
		a = append(a, arg{"--output", m.Output})
	}

	return
}

/*
MIDI in LilyPond-Quelltext umwandeln.

Options:
   -a, --absolute-pitches
                        Absolute Tonhöhen ausgeben
   -d, --duration-quant=DAUER
                        Notenlängen auf DAUER quantisieren
   -D, --debug          Drucken auf Fehler untersuchen
   -e, --explicit-durations
                        Explizite Notenlängen ausgeben
   -h, --help           diese Hilfe anzeigen und beenden
   -i, --include-header=DATEI
                        DATEI an die Ausgabe anfügen
   -k, --key=VORZ[:MOLL]
                        Tonart setzen: VORZ=+Kreuze|-B's; MOLL=1
   -o, --output=DATEI   Ausgabe in DATEI schreiben
   -p, --preview        Vorschau der ersten 4 Takte
   -q, --quiet          Fortschrittsnachrichten und Warnungen über
                        ausschweifende Stimmen unterdrücken
   -s, --start-quant=DAUER
                        Notenanfänge auf DAUER quantisieren
   -S, --skip           s anstelle von r für Pausen benutzen
   -t, --allow-tuplet=DAUER*ZÄHLER/NENNER
                        Wertaufteilungsdauern DAUER*ZÄHLER/NENNER erlauben
   -V, --verbose        wortreich sein
       --version        Versionsnummer ausgeben und beenden
   -w, --warranty       Informationen zu Gewährleistung und Copyright anzeigen
   -x, --text-lyrics    Jeden Text als Liedtext behandeln

Beispiele:

  $ midi2ly --key=-2:1 --duration-quant=32 --allow-tuplet=4*2/3 --allow-tuplet=2*4/3 foo.midi


*/
