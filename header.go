package lilypond

import (
	"bytes"
	"fmt"
)

type Header struct {
	elements []Element
}

func (l *Header) SetSubTitleOff() *Header {
	l.Add(Line("subtitle = ##f"))
	return l
}

func (l *Header) SetTaglineOff() *Header {
	l.Add(Line("tagline = ##f"))
	return l
}

func (l *Header) SetTagline(t string) *Header {
	l.Add(Line(fmt.Sprintf("tagline = %q", t)))
	return l
}

func (l *Header) SetBreakBefore() *Header {
	l.Add(Line("breakbefore = ##t"))
	return l
}

func (l *Header) SetSubSubTitle(t string) *Header {
	l.Add(Line(fmt.Sprintf("subsubtitle = %q", t)))
	return l
}

func (l *Header) SetComposer(c string) *Header {
	l.Add(Line(fmt.Sprintf("composer = %q", c)))
	return l
}

func (l *Header) SetPiece(p string) *Header {
	l.Add(Line(fmt.Sprintf("piece = %q", p)))
	return l
}

func (l *Header) SetOpus(o string) *Header {
	l.Add(Line(fmt.Sprintf("opus = %q", o)))
	return l
}

func (l *Header) SetDedication(d string) *Header {
	l.Add(Line(fmt.Sprintf("dedication = %q", d)))
	return l
}

func (l *Header) SetInstrument(d string) *Header {
	l.Add(Line(fmt.Sprintf("instrument = %q", d)))
	return l
}

func (l *Header) SetPoet(d string) *Header {
	l.Add(Line(fmt.Sprintf("poet = %q", d)))
	return l
}

func (l *Header) SetArranger(d string) *Header {
	l.Add(Line(fmt.Sprintf("arranger = %q", d)))
	return l
}

func (l *Header) SetCopyright(d string) *Header {
	l.Add(Line(fmt.Sprintf("copyright = %q", "© "+d)))
	return l
}

func (l *Header) SetSubTitle(t string) *Header {
	l.Add(Line(fmt.Sprintf("subtitle = %q", t)))
	return l
}

func (l *Header) SetTitle(t string) *Header {
	l.Add(Line(fmt.Sprintf("title = %q", t)))
	return l
}

func (l *Header) Add(elms ...Element) *Header {
	l.elements = append(l.elements, elms...)
	return l
}

func (l Header) String() string {
	if len(l.elements) == 0 {
		return ""
	}
	var bf bytes.Buffer

	bf.WriteString(`\header { ` + "\n")

	for _, elm := range l.elements {
		bf.WriteString(elm.String())
	}

	bf.WriteString("\n } \n")

	return bf.String()
}
