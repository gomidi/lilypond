// +build windows

package lilypond

import (
	"os/exec"
)

/*
func execCommand(c string) *exec.Cmd {
	//return exec.Command("powershell.exe", "/Command",  `$Process = [Diagnostics.Process]::Start("` + c + `") ; echo $Process.Id `)
	//return exec.Command("powershell.exe", "/Command", `$Process = [Diagnostics.Process]::Start("fluidsynth.exe", "-i -q -n $_file") ; echo $Process.Id `)
	fmt.Println(c)
	return exec.Command("cmd.exe", "/C", c)
}
*/

func cmdMidi2Ly(file string, args []string) *exec.Cmd {
	args = append(args, file)
	return exec.Command("midi2ly.exe", args...)
}

func cmdLilypond(file string, args []string) *exec.Cmd {
	args = append(args, file)
	return exec.Command("lilypond.exe", args...)
}

/*
func midiCatOutCmd(index int) *exec.Cmd {
	return exec.Command("midicat.exe", "out", fmt.Sprintf("--index=%v", index))
}

func midiCatInCmd(index int) *exec.Cmd {
	return exec.Command("midicat.exe", "in", fmt.Sprintf("--index=%v", index))
}

func midiCatCmd(args string) *exec.Cmd {
	//return execCommand("midicat.exe " + args)
	//fmt.Println("midicat.exe " + args)
	a := strings.Split(args, " ")
	return exec.Command("midicat.exe", a...)

}
*/
