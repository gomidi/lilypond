package lilypond

import (
	"bytes"
	"fmt"
)

/*
drumStyleTable = #timbales-style
drumStyleTable = #congas-style
drumStyleTable = #bongos-style
drumStyleTable = #percussion-style
*/

type DrumStaff struct {
	name   string
	voices []*Voice
	with   []Element
}

func (v *DrumStaff) With(s ...Element) *DrumStaff {
	v.with = append(v.with, s...)
	return v
}

func (l *DrumStaff) SetName(name string) *DrumStaff {
	l.name = name
	return l
}

func (l *DrumStaff) NewVoice() *Voice {
	v := &Voice{}
	v.isDrumvoice = true
	l.voices = append(l.voices, v)
	return v
}

func (p DrumStaff) isStaff() {}
func (p DrumStaff) String() string {
	if len(p.voices) == 0 {
		return ""
	}

	if len(p.voices) == 1 {
		var bf bytes.Buffer

		if p.name != "" {
			fmt.Fprintf(&bf, `\new DrumStaff = %q`, p.name)
		} else {
			bf.WriteString(`\new DrumStaff`)
		}

		if len(p.with) > 0 {
			fmt.Fprintf(&bf, ` \with { `+"\n")

			for _, w := range p.with {
				bf.WriteString(w.String())
			}

			bf.WriteString("\n } \n")
		}

		bf.WriteString(" { \n")

		bf.WriteString(`\drummode { ` + "\n")
		bf.WriteString(p.voices[0].String())
		bf.WriteString("\n } \n")
		bf.WriteString("\n } \n")

		return bf.String()
	}

	var bf bytes.Buffer

	if p.name != "" {
		fmt.Fprintf(&bf, `\new DrumStaff = %q << `+"\n", p.name)
	} else {
		bf.WriteString(`\new DrumStaff << ` + "\n")
	}

	for _, s := range p.voices {
		bf.WriteString(s.String())
	}

	bf.WriteString("\n >> \n")

	return bf.String()

}

/*
Schlagzeugsysteme
Ein Schlagzeug-System besteht üblicherweise aus einem Notensystem mit mehreren Linien, wobei jede Linie ein bestimmtes Schlagzeug-Instrument darstellt. Um die Noten darstellen zu können, müssen sie sich innerhalb von einem DrumStaff- und einem DrumVoice-Kontext befinden.

up = \drummode {
  crashcymbal4 hihat8 halfopenhihat hh hh hh openhihat
}
down = \drummode {
  bassdrum4 snare8 bd r bd sn4
}
\new DrumStaff <<
  \new DrumVoice { \voiceOne \up }
  \new DrumVoice { \voiceTwo \down }
>>

Das Beispiel zeigt ausdrücklich definierte mehrstimmige Notation. Die Kurznotation für mehrstimmige Musik, wie sie im Abschnitt Ich höre Stimmen beschrieben wird, kann auch verwendet werden.

\new DrumStaff <<
  \drummode {
    bd4 sn4 bd4 sn4
    << {
      \repeat unfold 16 hh16
    } \\ {
      bd4 sn4 bd4 sn4
    } >>
  }
>>

Geisternoten
Geisternoten für Schlagzeug und Perkussion können mit dem Klammer- (\parenthesize)-Befehl, beschrieben in Klammern, erstellt werden. Im Standard-\drummode-Modus ist aber das Parenthesis_engraver-Plugin nicht automatisch enthalten.

\new DrumStaff \with {
  \consists Parenthesis_engraver
}
<<
  \context DrumVoice  = "1" { s1 }
  \context DrumVoice  = "2" { s1 }
  \drummode {
    <<
      {
        hh8[ hh] <hh sn> hh16
        < \parenthesize sn > hh
        < \parenthesize sn > hh8 <hh sn> hh
      } \\
      {
        bd4 r4 bd8 bd r8 bd
      }
    >>
  }
>>
*/
