package lilypond

/*
Normale Wiederholungen
Die Syntax für normale Wiederholungen ist

\repeat Typ Wiederholungszähler musikAusdr
wobei musikAusdr ein musikalischer Ausdruck ist.

Wiederholung ohne alternativen Schluss:

\relative {
  \repeat volta 2 { c''4 d e f }
  c2 d
  \repeat volta 2 { d4 e f g }
}

Alternative Schlüsse können mit \alternative gesetzt werden. Damit die alternativen Schlüsse von den wiederholten Noten abgegrenzt werden, müssen sie in geschweiften Klammern zusammengefasst werden.

\repeat volta Wiederholungszähler musikAusdr
\alternative {
  { musikAusdr }
}

Wenn es mehr Wiederholungen gibt, als Alternativen angegeben sind, erhalten die ersten Wiederholungen den ersten Schluss.

Eine einfache Wiederholung mit einer Alternative:

\relative {
  \repeat volta 2 { c''4 d e f | }
  \alternative {
    { c2 e | }
    { f2 g | }
  }
  c1
}

Eine einfache Wiederholung mit mehr als einer Alternative:

\relative {
  \repeat volta 4 { c''4 d e f | }
  \alternative {
    { c2 e | }
    { f2 g | }
  }
  c1
}

Mehrfache Wiederholungen mit mehr als einer Alternative:

\relative {
  \repeat volta 3 { c''4 d e f | }
  \alternative {
    { c2 e | }
    { f2 g | }
    { a2 g | }
  }
  c1
}

Achtung: Wenn es zwei oder mehr Alternativen gibt, darf nichts zwischen der schließenden Klammer der einen und der öffnenden Klammer der anderen Wiederholung stehen, weil sonst nicht die erwartete Anzahl von Endungen produziert wird.

Achtung: Wenn man \relative innerhalb von \repeat notiert, ohne den Voice-Kontext explizit zu beginnen, erscheinen zusätzliche (ungewollte) Systeme. Siehe auch Ein zusätzliches System erscheint.

Wenn eine Wiederholung mitten in einem Takt beginnt und keine Alternativen hat, fällt normalerweise auch das Ende der Wiederholung mitten in einen Takt, sodass beide unvollständigen Takt einen vollständigen Takt ergeben. In diesem Fall bezeichnen die Wiederholungsstriche keine richtigen Taktstriche. Benutzen Sie nicht \partial-Befehle oder Taktüberprüfung, wo die Wiederholungslinien gesetzt werden:

\relative { % no \partial here
  c'4 e g  % no bar check here
  % no \partial here
  \repeat volta 4 {
    e4 |
    c2 e |
    % no \partial here
    g4 g g  % no bar check here
  }
  % no \partial here
  g4 |
  a2 a |
  g1 |
}

Ausgeschriebene Wiederholungen
Mit dem unfold-Befehl können Wiederholungen eingesetzt werden, um repetitive Musik zu notieren. Die Syntax ist

\repeat unfold Wiederholungszähler musikAusdr

https://lilypond.org/doc/v2.24/Documentation/notation/short-repeats

Prozent-Wiederholungen
Kurze wiederholte Muster werden einmal gesetzt und das wiederholte Muster wird durch ein besonderes Zeichen ersetzt.

Die Syntax lautet:

\repeat percent Wiederholungszahl musikAusdr
wobei musikAusdr ein musikalischer Ausdruck ist.



Muster, die kürzer als ein Takt sind, werden mit Schrägstrichen ersetzt:

\relative c'' {
  \repeat percent 4 { c128 d e f }
  \repeat percent 4 { c64 d e f }
  \repeat percent 5 { c32 d e f }
  \repeat percent 4 { c16 d e f }
  \repeat percent 4 { c8 d }
  \repeat percent 4 { c4 }
  \repeat percent 2 { c2 }
}

Muster von einem oder zwei Takten Dauer werden mit prozentartigen Symbolen ersetzt:

\relative c'' {
  \repeat percent 2 { c4 d e f }
  \repeat percent 2 { c2 d }
  \repeat percent 2 { c1 }
}

Prozent-Wiederholungen zählen
Ganztaktwiederholungen mit mehr als zwei Wiederholungen erhalten einen Zähler, wenn man die entsprechende Eigenschaft einsetzt:

\relative c'' {
  \set countPercentRepeats = ##t
  \repeat percent 4 { c1 }
}

Tremolo-Wiederholung
Tremolos können in zwei Arten notiert werden: als Wechsel zwischen zwei Noten oder Akkorden oder als schnelle Wiederholung einer einzigen Note. Tremolos, die als Wechsel realisiert werden, werden dargestellt, indem Balken zwischen die Noten gesetzt werden, Tremolos, die eine schnelle Wiederholung darstellen, haben Balken oder Schrägstriche am Hals einer einzigen Note.

Um Tremolobalken zwischen Noten zu setzen, kann der \repeat-Befehl mit dem Tremolo-Stil benutzt werden:

\relative c'' {
  \repeat tremolo 8 { c16 d }
  \repeat tremolo 6 { c16 d }
  \repeat tremolo 2 { c16 d }
}

Es gibt zwei Möglichkeiten, ein Tremolozeichen zu einer einzelnen Noten hinzuzufügen. Die \repeat tremolo-Syntax kann hier auch benutzt werden; in diesem Fall wird die Note allerdings nicht eingeklammert:

\repeat tremolo 4 c'16

Die gleiche Darstellung wird erreicht, indem nach der Note :Zahl geschrieben wird. Die Zahl zeigt die Dauer der Unterteilung an, und sie muss mindestens den Wert 8 haben. Ein Wert von 8 ergibt einen Balken durch den Notenhals. Wenn die Zahl ausgelassen wird, wird der letzte benutzte Wert eingesetzt (gespeichert in tremoloFlags):

\relative {
  c''2:8 c:32
  c: c:
}
*/
