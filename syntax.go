package lilypond

/*
{
  c' e' g' e'
}
*/

// c d e

/*
type LyFile struct {
	Path    path.Relative
	Content Content
}

func (ly LyFile) Save(fsys fs.ExtWriteable) error {
	return fsys.Write(ly.Path, fs.ReadCloser(strings.NewReader(ly.Content.String())), false)
}
*/

/*
	% Change to numeric style
  \numericTimeSignature
*/

/*
Fingersatzanweisungen
Fingersatzanweisungen können folgenderweise notiert werden: ‘Note’-Zahl

\relative { c''4-1 d-2 f-4 e-3 }

Für Fingerwechsel muss eine Textbeschriftung (markup) benutzt werden:

c4-1 d-2 f-4 c^\markup { \finger "2 - 3" }

Mit dem Daumen-Befehl (\thumb) können die Noten bezeichnet werden, die mit dem Daumen (etwa auf dem Cello) gespielt werden sollen.

\relative { <a'_\thumb a'-3>2 <b_\thumb b'-3> }

Fingersätze für Akkorde können auch zu einzelnen Noten hinzugefügt werden, indem sie innerhalb der Akkord-Klammer direkt an die Noten angefügt werden.

\relative {
  <c''-1 e-2 g-3 b-5>2 <d-1 f-2 a-3 c-5>
}

Fingersatz auch innerhalb des Systems setzen
Normalerweise werden vertikal orientierte Fingersatzzahlen außerhalb des Systems gesetzt. Das kann aber verändert werden.

\relative c' {
  <c-1 e-2 g-3 b-5>2
  \override Fingering.staff-padding = #'()
  <c-1 e-2 g-3 b-5>4 g'-0
  a8[-1 b]-2 g-0 r
  \override Fingering.add-stem-support = ##f
  a[-1 b]-2 g-0 r
  \override Fingering.add-stem-support = ##t
  a[-1 b]-2 g-0 r
  \override Fingering.add-stem-support = #only-if-beamed
  a[-1 b]-2 g-0 r
}
*/

/*
Marken benutzen
Der \tag #'TeilA-Befehl markiert einen musikalischen Ausdruck mit der Bezeichnung TeilA. Ausdrücke, die auf diese Weise markiert werden, können mit ihrer Bezeichnung später ausgewählt bzw. ausgefiltert werden. Das geschieht mit den Befehlen \keepWithTag #'Bezeichnung bzw. \removeWithTag #'Bezeichnung. Die Wirkung dieser Filter auf die markierten Notenabschnitte ist wie folgt:

Filter	Resultat
Markierte Noten mit vorgesetztem \keepWithTag #'Bezeichnung	Unmarkierte Noten und Noten mit der Marke Bezeichnung werden gesetzt, Noten mit einer anderen Marke werden nicht angezeigt.
Markierte Noten mit vorgesetztem \removeWithTag #'Bezeichnung	Unmarkierte Noten und Noten mit einer anderen Marke als Bezeichnung wird angezeigt, Noten markiert mit Bezeichnung werden nicht angezeigt.
Markierte Noten, weder mit vorgesetztem \keepWithTag noch \removeWithTag	Alle markierten und unmarkierten Noten werden angezeigt.
Die Argumente der Befehle \tag, \keepWithTag und \removeWithTag sollten ein Symbol sein (wie etwa #'score oder #'part), gefolgt von einem musikalischen Ausdruck.

Im folgenden Beispiel erscheinen zwei Versionen der Noten, eine zeigt Triller in normaler Notation, die andere zeigt sie ausgeschrieben:

music = \relative {
  g'8. c32 d
  \tag #'trills { d8.\trill }
  \tag #'expand { \repeat unfold 3 { e32 d } }
  c32 d
 }

\score {
  \keepWithTag #'trills \music
}
\score {
  \keepWithTag #'expand \music
*/

/*
befehlsübersicht

die wichtigsten
https://lilypond.org/doc/v2.24/Documentation/notation/cheat-sheet

alle
https://lilypond.org/doc/v2.24/Documentation/notation/lilypond-command-index
*/

/*
Die neuen Befehle \textMark und \textEndMark sind verfügbar, um einen beliebiges Stück Text zwischen Noten einzufügen, genannt eine Textmarkierung. Diese Befehle verbessern die bisher verfügbare Syntax mit dem Befehl \mark, aufgerufen als \mark markup (\mark "…" oder \mark \markup …).
\fixed c' {
  \textMark "Text mark"
  c16 d e f e f e d c e d c e d c8
  \textEndMark "Text end mark"
}
*/

/*
https://lilypond.org/doc/v2.24/Documentation/notation/writing-pitches

Versetzungszeichen
Achtung: Neue Benutzer sind manchmal verwirrt, wie Versetzungszeichen und Vorzeichen/Tonarten funktionieren.
In LilyPond sind Notenbezeichnungen die wirkliche Tonhöhe, erst durch Vorzeichen wird bestimmt, wie diese Tonhöhe dann im Notenbild dargestellt wird.
Eine einfache Tonhöhe wie etwa c bedeutet also immer das eingestrichene C ohne Versetzungszeichen, egal was für Vorzeichen/Tonart oder Schlüssel gesetzt sind.
Mehr Information dazu in Tonhöhen und Tonartbezeichnungen (Vorzeichen).

Ein Kreuz wird eingegeben, indem man -is an die Notenbezeichnung hängt, ein b durch -es.
Doppelkreuze und Doppel-Bs werden durch Hinzufügen von -isis und -eses hinter die Notenbezeichnung erzeugt.
Diese Syntax leitet sich von den holländischen Notenbezeichnungen ab.
Um andere Bezeichnungen für Versetzungszeichen zu benutzen, siehe Notenbezeichnungen in anderen Sprachen.

\relative c'' { ais1 aes aisis aeses }

Auch die deutschen Varianten as für aes und es für ees sind erlaubt.
Im Unterschied zum Deutschen ist aber bes die einzige Version für den Ton B, während his als bis geschrieben werden muss.
Das kann aber auch verändert werden, siehe Notenbezeichnungen in anderen Sprachen.

Ein Auflösungszeichen macht die Wirkung eines Kreuzes oder Bs rückgängig.
Diese Auflösungszeichen werden jedoch nicht als Suffix einer Tonhöhenbezeichnung eingegeben, sondern sie ergeben sich (automatisch) aus dem Kontext,
wenn die nicht alterierte Notenbezeichnung eingegeben wird.

Versetzungszeichen für Vierteltöne werden durch Anhängen der Endungen -eh (Erniedrigung) und -ih (Erhöhung) an den Tonhöhenbuchstaben erstellt.
Das Beispiel zeigt eine in Vierteltönen aufsteigende Serie vom eingestrichenen C.

\relative c'' { ceseh1 ces ceh c cih cis cisih }

Normalerweise werden Versetzungszeichen automatisch gesetzt, aber sie können auch manuell hinzugefügt werden.
Ein erinnerndes Versetzungszeichen kann erzwungen werden, indem man ein Ausrufungszeichen (!) hinter die Notenbezeichnung schreibt.
Ein warnendes Versetzungszeichen (also ein Vorzeichen in Klammern) wird durch Anfügen eines Fragezeichens (?) erstellt.
Mit diesen zusätzlichen Zeichen kann man sich auch Auflösungszeichen ausgeben lassen.

Verhindern, dass zusätzliche Auflösungszeichen automatisch
hinzugefügt werden

*/

/*
<>^\markup { [MAJOR GENERAL] }
<>_\markup { \italic { Cue: ... it is yours } }
*/

// \autoBeamOn, \autoBeamOff, \dotsUp, \dotsDown, \dotsNeutral.

// see https://lilypond.org/doc/v2.24/Documentation/notation/writing-rhythms

// Alternative breve notes
// \override Staff.NoteHead.style = #'altdefault
// \override Staff.NoteHead.style = #'baroque
// \revert Staff.NoteHead.style

// \override Dots.dot-count = #4
// \override Dots.dot-count = #0
// \revert Dots.dot-count

/*
Triolen und andere rhythmische Aufteilungen werden aus einem musikalischen Ausdruck erstellt, indem dessen Tondauern mit einem Bruch multipliziert werden.

\times Bruch musikalischer Ausdruck
Die Dauer eines musikalischen Ausdrucks wird mit dem Bruch multipliziert. Der Nenner des Bruchs wird über (oder unter) den Noten ausgegeben, optional mit einer eckigen Klammer, die die Noten einfasst. Die üblichste Aufteilung ist die Triole, in welcher drei Noten die Länge von zwei haben, der Wert jeder einzelnen Note ist also 2/3 der notierten Länge.

a2 \tuplet 3/2 { b4 b b }
c4 c \tuplet 3/2 { b4 a g }

\tupletUp, \tupletDown, \tupletNeutral

\override TupletNumber.text = #tuplet-number::calc-fraction-text

\omit TupletNumber

Bindebögen
Ein Bindebogen verbindet zwei benachbarte Noten der selben Tonhöhe. Als Resultat wird die Dauer der Notenlänge verlängert.

Achtung: Bindebögen (engl. tie) dürfen nicht mit Legatobögen (engl. slur) verwechselt werden, durch die die Vortragsart bezeichnet wird, noch mit Phrasierungsbögen (engl. phrasing slur), die musikalische Phrasen anzeigen. Ein Bindebogen ist nur eine Art, die Tondauer zu verlängern, ähnlich etwa wie die Punktierung.

Ein Bindebogen wird mit der Tilde ~ (AltGr++) notiert.

a2 ~ 2

<c e g> ~ <c e g>

*/

// proportionalNotationDuration = #(ly:make-moment 1/20)
// \override SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/64)

// \glissando nach der Note, z.B.

/*
https://lilypond.org/doc/v2.25/Documentation/notation/glissando.es.html

\relative {
  g'2\glissando g'
  c2\glissando c,
  \afterGrace f,1\glissando f'16
}
*/

/*
\key Tonhöhe Modus
Der Wert Modus sollte entweder \major oder \minor sein, um Moll oder Dur der Tonhöhe zu erhalten.
Es können auch Modusbezeichnungen für Kirchentonarten verwendet werden:
\ionian (Ionisch),
\locrian (Lokrisch),
\aeolian (Aeolisch),
\mixolydian (Mixolydisch),
\lydian (Lydisch),
\phrygian (Phrygisch) und
\dorian (Dorisch).

\relative {
  \key g \major
  fis''1
  f
  fis
}
*/

/*
\change Staff = up
\change Staff = down
*/

/*
Bekannte Probleme und Warnungen
Gleichzeitig erklingende Noten werden bei der automatischen Bestimmung der Versetzungszeichen nicht berücksichtigt: nur die vorige Note und die Vorzeichen werden einbezogen. Man muss die Versetzungszeichen mit ! oder ? schreiben, wenn gleichzeitig unterschiedliche Alterationen vorkommen, wie etwa für ‘<f! fis!>’.

*/

/*
Tonumfang
Der Begriff ambitus (Pl. ambitus) beschreibt den Stimmumfang einer Stimme. Er kann auch die Töne bedeuten, die ein Musikinstrument zu spielen in der Lage ist. Ambitus werden in Chorpartituren gesetzt, damit die Sänger schnell wissen, ob sie die Stimme meistern können.

Ambitus werden zu Beginn des Stückes nahe des ersten Schlüssels notiert. Der Stimmumfang wird durch zwei Notenköpfe dargestellt, die die tiefste und höchste Note der Stimme repräsentieren. Versetzungszeichen werden nur gesetzt, wenn sie nicht durch die Tonart definiert werden.

\layout {
  \context {
    \Voice
    \consists Ambitus_engraver
  }
}

\relative {
  aes' c e2
  cis,1
}
*/

/*
https://lilypond.org/doc/v2.24/Documentation/notation/displaying-pitches#clef

Ambitus pro Stimme hinzufügen
Ambitus können pro Stimme gesetzt werden. In diesem Fall müssen sie manuell verschoben werden, um Zusammenstöße zu verhindern.

\new Staff <<
  \new Voice \with {
    \consists "Ambitus_engraver"
  } \relative c'' {
    \override Ambitus.X-offset = #2.0
    \voiceOne
    c4 a d e
    f1
  }
  \new Voice \with {
    \consists "Ambitus_engraver"
  } \relative c' {
    \voiceTwo
    es4 f g as
    b1
  }
>>
*/

/*
Setzen der Taktnummern in regelmäßigen Intervallen
Taktnummern können in regelmäßigen Intervallen gesetzt werden, indem man die Eigenschaft barNumberVisibility definiert. In diesem Beispiel werden die Taktnummern jeden zweiten Takt gesetzt, außer am Ende einer Zeile.

\relative c' {
  \override Score.BarNumber.break-visibility = #end-of-line-invisible
  \set Score.currentBarNumber = #11
  % Print a bar number every second measure
  \set Score.barNumberVisibility = #(every-nth-bar-number-visible 2)
  c1 | c | c | c | c
  \break
  c1 | c | c | c | c
}
*/

// \chordmode { c1 }
// \transpose f g {
/*
fis
  c2 ges

	c-sharp
	d-flat
*/

/*
https://lilypond.org/doc/v2.24/Documentation/notation/bars#bar-numbers

Übungszeichen können mit dem \mark-Befehl ausgegeben werden:

c1 \mark \default
c1 \mark \default
c1 \mark #8
c1 \mark \default
c1 \mark \default

\relative c'' {
  \set Score.rehearsalMarkFormatter = #format-mark-box-alphabet
  c1 \mark \default
  c1 \mark \default
  c1 \mark #8
  c1 \mark \default
  c1 \mark \default
}

Musikbuchstaben (wie etwa das Segno-Zeichen) können mit dem Befehl \musicglyph als ein \mark-Zeichen definierte werden:

\relative c' {
  c1 \mark \markup { \musicglyph "scripts.segno" }
  c1 \mark \markup { \musicglyph "scripts.coda" }
  c1 \mark \markup { \musicglyph "scripts.ufermata" }
  c1
}
*/

/*
\score {
  \new Staff {
*/

/*
\relative <<
  \new Staff {
    \time 3/4
    c'4 c c |
    c4 c c |
  }
  \new Staff {
    \time 3/4
    \set Staff.timeSignatureFraction = 9/8
    \scaleDurations 2/3 {
      \repeat unfold 3 { c8[ c c] }
      \repeat unfold 3 { c4 c8 }
    }
  }
  \new Staff {
    \time 3/4
    \set Staff.timeSignatureFraction = 10/8
    \scaleDurations 3/5 {
      \repeat unfold 2 { c8[ c c] }
      \repeat unfold 2 { c8[ c] } |
      c4. c4. \tuplet 3/2 { c8[ c c] } c4
    }
  }
>>
*/

/*
https://lilypond.org/doc/v2.24/Documentation/notation/displaying-rhythms#time-signature

Automatische Aufteilung von Noten
Lange Noten, die über Taktlinien hinüberreichen, können automatisch in übergebundene Noten aufgeteilt werden. Dieses Verhalten erreicht man, indem der Note_heads_engraver mit dem Completion_heads_engraver ausgetauscht wird. Auf gleiche Art können lange Pausen, die über Taktgrenzen reichen, automatisch aufgeteilt werden, indem man den Rest_engraver mit dem Completion_rest_engraver ersetzt. Im nächsten Beispiel werden Noten und Pausen, die über die Taktlinie dauern, aufgeteilt; Noten werden auch übergebunden.

\new Voice \with {
  \remove Note_heads_engraver
  \consists Completion_heads_engraver
  \remove Rest_engraver
  \consists Completion_rest_engraver
}
\relative {
  c'2. c8 d4 e f g a b c8 c2 b4 a g16 f4 e d c8. c2 r1*2
}
*/

/*
Melodierhythmus anzeigen
Manchmal soll nur der Rhythmus einer Melodie dargestellt werden. Das erreicht man mit einem Rhythmus-Notensystem. Alle Tonhöhen werden auf eine Linie reduziert und das System hat auch nur eine einzige Linie.

<<
  \new RhythmicStaff {
    \new Voice = "myRhythm" \relative {
      \time 4/4
      c'4 e8 f g2
      r4 g g f
      g1
    }
  }
  \new Lyrics {
    \lyricsto "myRhythm" {
      This is my song
      I like to sing
    }
  }
>>
*/

/*
Textbeschriftung (Einleitung)
Eine \markup-Umgebung wird benutzt, um Text mit einer großen Anzahl von Formatierungsmöglichkeiten (im „markup-Modus“) zu setzen.

Die Syntax für Textbeschriftungen ähnelt der normalen Syntax von LilyPond: ein \markup-Ausdruck wird in geschweifte Klammern eingeschlossen ({… }). Ein einzelnes Wort wird als ein Minimalausdruck erachtet und muss deshalb nicht notwendigerweise eingeklammert werden.

Anders als Text in Anführungsstrichen können sich in einer Textbeschriftungsumgebung (\markup) geschachtelte Ausdrücke oder weitere Textbefehle befinden, eingeführt mit einem Backslash (\). Derartige Befehle beziehen sich nur auf den ersten der folgenden Ausdrücke.

\relative {
  a'1-\markup intenso
  a2^\markup { poco \italic più forte  }
  c e1
  d2_\markup { \italic "string. assai" }
  e
  b1^\markup { \bold { molto \italic  agitato } }
  c
}

Eine \markup-Umgebung kann auch Text in Anführungszeichen beinhalten. Derartige Zeichenketten werden als ein Textausdruck angesehen, und darum werden innerhalb von ihnen Befehle oder Sonderzeichen (wie \ oder #) so ausgegeben, wie sie eingeben werden. Doppelte Anführungsstriche können gesetzt werden, indem man ihnen einen Backslash voranstellt.

\relative {
  a'1^"\italic Text..."
  a_\markup { \italic "... setzt \"kursive\" Buchstaben!" }
  a a
}
*/

/*
https://lilypond.org/doc/v2.24/Documentation/notation/special-rhythmic-concerns#grace-notes

Verzierungen, mit dem Befehl \grace notiert, sind ausgeschriebene Ornamente. Sie werden in einer kleineren Schriftgröße gesetzt und nehmen keine logische Zeit im Takt ein.

\relative {
  c''4 \grace b16 a4(
  \grace { b16 c16 } a2)
}
[image of music]

Es gibt drei Arten von Verzierungen: den Vorschlag (engl. acciaccatura), eine angebundene Verzierungsnote mit einem Schrägstrich durch den Hals, und den Vorhalt (engl. appoggiatura), welcher den Wert der Hauptnote um seinen eigenen Wert verkürzt und ohne Schrägstrich notiert wird. Man kann einen Vorschlag auch mit Schrägstrich durch den Hals, aber ohne Legatobogen notieren. Diese Verzierung wird mit dem Befehl \slashedGrace notiert und wird zwischen Noten notiert, die selber einen Legatobogen haben.

\relative {
  \acciaccatura d''8 c4
  \appoggiatura e8 d4
  \acciaccatura { g16 f } e2
  \slashedGrace a,8 g4
  \slashedGrace b16 a4(
  \slashedGrace b8 a2)
}

Wenn Sie eine Note mit einer Verzierung abschließen wollen, müssen Sie den \afterGrace-Befehl benutzen. Er benötigt zwei Argumente: die Hauptnote und die Verzierung, die nach der Hauptnote folgen soll:

\relative { c''1 \afterGrace d1 { c16[ d] } c1 }
[image of music]

Damit wird die Verzierung mit einem Abstand von der Hauptnote gesetzt, der 3/4 der Dauer der Hauptnote entspricht. Dieser Standard kann durch Definition von afterGraceFraction verändert werden. Das nächste Beispiel zeigt, wie sich der Abstand verändert, wenn der Wert 3/4, 15/16 und 1/2 der Hauptnote beträgt.

<<
  \new Staff \relative {
    c''1 \afterGrace d1 { c16[ d] } c1
  }
  \new Staff \relative {
    #(define afterGraceFraction (cons 15 16))
    c''1 \afterGrace d1 { c16[ d] } c1
  }
  \new Staff \relative {
    #(define afterGraceFraction (cons 1 2))
    c''1 \afterGrace d1 { c16[ d] } c1
  }
>>

Der Abstand zwischen der Hauptnote und der Verzierung kann auch mit unsichtbaren Noten beeinflusst werden. Im nächsten Beispiel wird die Verzierung mit einem Abstand von 7/8 zur Hauptnote gesetzt.

\new Voice {
  << { d1^\trill_( }
     { s2 s4. \grace { c16 d } } >>
  c1)
}
*/

/*

Systeme trennen
Wenn die Anzahl der Systeme sich von Seite zu Seite ändert, wird normalerweise ein Trennzeichen hinzugefügt, dass die Systeme voneinander trennt. Die Standardeinstellung ist, dass der Trenner nicht gesetzt wird, aber man kann ihn mit einer Option in der \paper-Umgebung angeschalten.

\book {
  \score {
    \new StaffGroup <<
      \new Staff {
        \relative {
          c''4 c c c
          \break
          c4 c c c
        }
      }
      \new Staff {
        \relative {
          c''4 c c c
          \break
          c4 c c c
        }
      }
    >>
  }
  \paper {
    system-separator-markup = \slashSeparator
    % following commands are needed only to format this documentation
    paper-width = 100\mm
    paper-height = 100\mm
    tagline = ##f
  }
}
*/

/*
Das Papierformat einstellen
‚A4‘ ist der Standardwert, wenn keine ausdrückliches Papierformat eingestellt ist. Es gibt jedoch zwei Funktionen, mit denen man das Papierformat ändern kann: set-default-paper-size

#(set-default-paper-size "quarto")
welcher immer auf oberster Ebene der Datei geschrieben werden muss, und set-paper-size

\paper {
  #(set-paper-size "tabloid")
}
*/

/*
4.2.1 Die \layout-Umgebung
Während die \paper-Umgebung Einstellungen für die Formatierung der Seiten eines gesamten Dokuments enthalten, enthält die \layout-Umgebung Einstellungen für einzelne Partituren. Um Layoutoptionen für Partituren global einzustellen, müssen sie in einer \layout-Umgebung gesetzt werden, die sich auf höchster Ebene in der Datei befindet. Um sie für einzelne Partituren festzulegen, muss die \layout-Umgebung innerhalb der \score-Umgebung nach den Noten eingetraten werden. Einstellungen, die in einer \layout-Umgebung vorkommen können, beinhalten:

die layout-set-staff-size-Scheme-Funktion,
Kontextveränderungen in \context-Umgebungen und
\paper-Variablen, die das Aussehen einer Partitur beeinflussen.
Die layout-set-staff-size-Funktion wird im nächsten Abschnitt behandelt, Die Notensystemgröße einstellen. Kontextveränderungen werden in einem eigenen Kapitel behandelt, siehe Umgebungs-Plugins verändern and Die Standardeinstellungen von Kontexten ändern. Die \paper-Variablen, die innerhalb der \layout-Umgebungen erlaubt sind, sind:

line-width, ragged-right und ragged-last (siehe \paper-Variablen für Breite und Ränder)
indent und short-indent (siehe \paper-Variablen für Verschiebungen und Einrückungen)
system-count (siehe \paper-Variablen für den Zeilenumbruch)
Hier ist ein Beispiel für eine \layout-Umgebung:

\layout {
  indent = 2\cm
  \context {
    \StaffGroup
    \override StaffGrouper.staff-staff-spacing.basic-distance = #8
  }
  \context {
    \Voice
    \override TextScript.padding = #1
    \override Glissando.thickness = #3
  }
}
*/

// \new Staff = "Tenor" {

/*
\book {
  \score {
    \new Staff {
      \new Voice {
        { c'4 d' e'2 }
      }
    }
    \layout { }
  }
  \paper { }
  \header { }
}
\book {
  \score {
    \new Staff {
      \new Voice {
        \relative {
          c''4 a b c
        }
      }
    }
    \layout { }
  }
}
*/

/*
Wie funktioniert die Titel-Umgebung?
Es gibt zwei Arten von Titelumgebungen: die Hauptumgebung, die über der ersten \score-Umgebung innerhalb eines „book“ notiert wird, und individuelle Titelumgebungen, die innerhalb von \score auftreten können. Textfelder für beide Typen werden in der \header-Umgebung eingegeben.

Wenn in dem „book“ nur eine einzelne Partitur vorkommt, kann die \header-Umgebung innheralb oder außerhalb der \score-Umgebung geschrieben werden.

\header {
  title = "SUITE I."
  composer = "J. S. Bach."
}

\score {
  \header {
    piece = "Prélude."
  }
  \new Staff \relative {
    \clef bass
    \key g \major
    \repeat unfold 2 { g,16( d' b') a b d, b' d, } |
    \repeat unfold 2 { g,16( e' c') b c e, c' e, } |
  }
}

\score {
  \header {
    piece = "Allemande."
  }
  \new Staff \relative {
    \clef bass
    \key g \major
    \partial 16 b16 |
    <g, d' b'~>4 b'16 a( g fis) g( d e fis) g( a b c) |
    d16( b g fis) g( e d c) b(c d e) fis( g a b) |
  }
}

\book {
  \paper {
    print-all-headers = ##t
  }
  \header {
    title = "DAS WOHLTEMPERIRTE CLAVIER"
    subtitle = "TEIL I"
    % Do not display the tagline for this book
    tagline = ##f
  }
  \markup { \vspace #1 }
  \score {
    \header {
      title = "PRAELUDIUM I"
      opus = "BWV 846"
      % Do not display the subtitle for this score
      subtitle = ##f
    }
    \new PianoStaff <<
      \new Staff { s1 }
      \new Staff { \clef "bass" s1 }
    >>
  }
  \score {
    \header {
      title = "FUGA I"
      subsubtitle = "A 4 VOCI"
      opus = "BWV 846"
      % Do not display the subtitle for this score
      subtitle = ##f
    }
    \new PianoStaff <<
      \new Staff { s1 }
      \new Staff { \clef "bass" s1 }
    >>
  }
}

\book {
  \header {
      % The following fields are centered
    dedication = "Dedication"
    title = "Title"
    subtitle = "Subtitle"
    subsubtitle = "Subsubtitle"
      % The following fields are evenly spread on one line
      % the field "instrument" also appears on following pages
    instrument = \markup \with-color #green "Instrument"
    poet = "Poet"
    composer = "Composer"
      % The following fields are placed at opposite ends of the same line
    meter = "Meter"
    arranger = "Arranger"
      % The following fields are centered at the bottom
    tagline = "tagline goes at the bottom of the last page"
    copyright = "copyright goes at the bottom of the first page"
  }
  \score {
    \header {
        % The following fields are placed at opposite ends of the same line
      piece = "Piece 1"
      opus = "Opus 1"
    }
    { s1 }
  }
  \score {
    \header {
        % The following fields are placed at opposite ends of the same line
      piece = "Piece 2 on the same page"
      opus = "Opus 2"
    }
    { s1 }
  }
  \pageBreak
  \score {
    \header {
        % The following fields are placed at opposite ends of the same line
      piece = "Piece 3 on a new page"
      opus = "Opus 3"
    }
    { s1 }
  }
}

Beachten Sie:

Die Instrumentenbezeichnung wird auf jeder Seite wiederholt.
Nur piece (Stück)uand opus werden für eine Partitur (\score) gesetzt, wenn die \paper-Variable print-all-headers auf ##f gesetzt ist (Standardeinstellung).
Textfelder, die in einer \header-Umgebung nicht benutzt werden, werden durch \null-Textbeschriftung ersetzt, sodass sie keinen leeren Platz belegen.
Die Standardeinstellungen von scoreTitleMarkup platzieren die Felder piece (Stück) und opus zu den gegenüberliegenden Seiten der selben Zeile.

Mit der Variable breakbefore innerhalb einer \header-Umgebung, die für sich auch eine \score-Umgebung darstellt, kann man die Hauptüberschriften auf der ersten Seite allein ausgeben, sodass die Noten (in der score-Umgebung definiert) erst auf der folgenden Seite beginnen.

\book {
  \header {
    title = "This is my Title"
    subtitle = "This is my Subtitle"
    copyright = "This is the bottom of the first page"
  }
  \score {
    \header {
      piece = "This is the Music"
      breakbefore = ##t
    }
    \repeat unfold 4 { e'' e'' e'' e'' }
  }
}

Standardlayout von Kopf- und Fußzeilen
Kopf- und Fußzeilen sind Textzeilen, die ganz oben und ganz unten auf der Seite stehen, unabhängig vom Textbereich eines Buches. Sie können mit folgenden \paper-Variablen kontrolliert werden:

oddHeaderMarkup
evenHeaderMarkup
oddFooterMarkup
evenFooterMarkup
Diese Beschriftungsvariablen können nur auf Textfelder einer Haupttitelumgebung (eine \header-Umgebung auf höchster Ebene, die sich auf alle \score-Umgebungen einer Datei bezieht) zugreifen und sind definiert in der Datei ‘ly/titling-init.ly’. In den Standardeinstellungen

werden Seitenzahlen automatisch ganz oben links (wenn gerade) oder ganz oben rechts (wenn ungerade) gesetzt, beginnend mit der zweiten Seite.
wird das instrument-Textfeld auf jeder Seite zentriert, beginnend mit der zweiten Seite.
wird der copyright-Text unten auf der ersten Zeite zentriert.
wird der Inhalt von tagline unten auf der letzten Seite zentriert und unterhalb des copyright-Texts, wenn es sich nur um eine Seite handelt.

Die Standardeinstellung von tagline kann verändert werden, indem man ein tagline-Feld in die \header-Umgebung auf höchster Ebene schreibt.

\book {
  \header {
    tagline = "... music notation for Everyone"
  }
  \score {
    \relative {
      c'4 d e f
    }
  }
}

Um die tagline ganz zu entfernen, wird ihr Inhalb als ##f (falsch) definiert.
*/

/*
Struktur einer Partitur
Eine \score-Umgebung muss einen einzelnen musikalischen Ausdruck beinhalten, der durch geschweifte Klammern begrenzt wird:

\score {
...
}

Achtung: Es darf nur ein äußerer musikalischer Ausdruck in der \score-Umgebung geschrieben werden, und er muss von geschweiften Klammern umgeben sein.

Dieser einzelne musikalische Ausdruck kann beliebige Größe annehmen und andere musikalische Ausdrücke von beliebiger Komplexität beinhalten. Alle diese Beispiele sind musikalische Ausdrücke:


{ c'4 c' c' c' }


{
  { c'4 c' c' c' }
  { d'4 d' d' d' }
}


<<
  \new Staff { c'4 c' c' c' }
  \new Staff { d'4 d' d' d' }
>>

{
  \new GrandStaff <<
    \new StaffGroup <<
      \new Staff { \Flöte }
      \new Staff { \Oboe }
    >>
    \new StaffGroup <<
      \new Staff { \GeigeI }
      \new Staff { \GeigeII }
    >>
  >>
}

Kommentare bilden eine Ausnahme dieser Regel. (Andere Ausnahmen siehe Die Dateistruktur.) Sowohl einzeilige als auch Blockkommentare (eingegrenzt durch %{ .. %}) können an beliebiger Stelle einer Eingabedatei geschrieben werden. Sie können innerhalb oder außerhalb der \score-Umgebung vorkommen, und innerhalb oder außerhalb des einzelnen musikalischen Ausdrucks innerhalb der \score-Umgebung.

Denken Sie daran, dass auch eine Datei, die nur eine \score-Umgebung enhält, implizit in eine \book-Umgebung eingeschlossen wird. Eine \book-Umgebung in einer Eingabdatei produziert wenigstens eine Ausgabedatei, und standardmäßig wird der Name der Ausagabedatei aus dem Namen der Eingabedatei abgeleitet. ‘fandangoforelephants.ly’ produziert also ‘fandangoforelephants.pdf’.

Mehrere Partituren in einem Buch
Eine Partitur kann mehrere musikalische Stücke und verschiedene Texte beinhalten. Beispiele hierzu sind etwa eine Etüdensammlung oder ein Orchesterstück mit mehreren Sätzen. Jeder Satz wird in einer eigenen \score-Umgebung notiert:

\score {
  ..Noten..
}
und Texte werden mit einer \markup-Umgebung geschrieben:

\markup {
  ..Text..
}
Alle Sätze und Texte, die in derselben ‘.ly’-Datei vorkommen, werden normalerweise in eine einzige Ausgabedatei gesetzt.

\score {
  ..
}
\markup {
  ..
}
\score {
  ..
}
Eine wichtige Ausnahme stellen Dokumente dar, die mit lilypond-book erstellt werden, für die Sie explizit \book-Umgebungen notieren müssen, weil sonst nur die erste \score- bzw. \markup-Umgebung angezeigt wird.

Der Kopfbereich für jedes Musikstück kann innerhalb der \score-Umgebung definiert werden. Die piece-(Stück)-Bezeichnung aus dieser \header-Umgebung wird vor jedem Satz ausgegeben. Die Überschrift für ein ganzes Buch kann innerhalb von \book notiert werden, aber wenn diese Umgebung fehlt, wird die \header-Umgebung genommen, die auf erster Ebene der Datei notiert ist.

\header {
  title = "Acht Miniaturen"
  composer = "Igor Stravinsky"
}
\score {
  \header { piece = "Romanze" }
  …
}
\markup {
   ..Text der zweiten Strophe..
}
\markup {
   ..Text der dritten Strophe..
}
\score {
  \header { piece = "Menuetto" }
  …
}

Stücke können innerhalb eines Buches mit \bookpart gruppiert werden. Derartige Buchabschnitte werden durch einen Seitenumbruch voneinander getrennt und können wie auch das ganze Buch selber mit einem Titel innerhalb einer \header-Umgebung beginnen.

\bookpart {
  \header {
    title = "Buchtitel"
    subtitle = "Erster Teil"
  }
  \score { … }
  …
}
\bookpart {
  \header {
    subtitle = "Zweiter Teil"
  }
  \score { … }
  …
}

Mehrere Ausgabedateien aus einer Eingabedatei
Wenn Sie mehrere Ausgabedateien aus derselben ‘.ly’-Datei haben wollen, können Sie mehrere \book-Umgebungen hinzufügen, wobei jede Umgebung eine neue Ausgabedatei produziert. Wenn Sie keine \book-Umgebung in der Eingabedatei angeben, wird die Datei von LilyPond implizit als eine große \book-Umgebung behandelt, siehe auch Die Dateistruktur.

Wenn man mehrere Dateien aus einer einzigen Eingabedatei erstellt, stellt LilyPond sicher, dass keine der Ausgabedateien der vorhandenen \book-Umgebungen eine andere Ausgabedatei, etwa von der vorherigen \book-Umgebung, überschreibt.

Dies geschieht, indem ein Suffix an den Ausgabenamen für jede \book-Umgebung gehängt wird, die den Dateinamen der Eingabdatei als Grundlage nimmt.

Das Standardverhalten ist es, einen Zahlen-Suffix für die Namen hinzuzufügen, die in Konflikt stehen. Der Code

\book {
  \score { … }
  \layout { … }
}
\book {
  \score { … }
  \layout { … }
}
\book {
  \score { … }
  \layout { … }
}

Die Dateistruktur
Eine ‘.ly’-Datei kann eine beliebige Anzahl an Ausdrücken auf der obersten Ebene beinhalten, wobei ein Ausdruck der obersten Ebene einer der folgenden sein kann:

Eine Ausgabedefinition, wie \paper, \midi und \layout. Derartige Definitionen auf oberster Ebene verändern die globalen Einstellungen für das ganze „Buch“. Wenn mehr als eine derartige Definition desselben Typs auf oberster Ebene angegeben wird, hat die spätere Vorrang. Für Einzelheiten, wie dadurch die \layout-Umgebung beeinflusst wird, siehe Die \layout-Umgebung.
Ein direkter Scheme-Ausdruck, wie etwa #(set-default-paper-size "a7" 'landscape) oder #(ly:set-option 'point-and-click #f).
Eine \header-Umgebung. Damit wird die globale Titelei eingestellt. Das ist die Umgebung, in der sich Definition für das ganze Buch befinden, wie Komponist, Titel usw.
Eine \score-Umgebung. Die in ihr enthaltene Partitur wird zusammen mit anderen vorkommenden \score-Umgebungen gesammelt und in ein \book zusammengefasst. Dieses Verhalten kann verändert werden, indem die Variable toplevel-score-handler auf höchster Ebene gesetzt wird. Die Definition des Standards findet sich in der Datei ‘../scm/lily.scm’.
Eine \book-Umgebung fasst mehrere Sätze (d. h. mehrere \score-Umgebungen) logisch in ein Dokument zusammen. Wenn mehrere \score-Partituren vorkommen, wird für jede \book-Umgebung eine eigene Ausgabedatei erstellt, in der alle in der Umgebung enthaltenen Partituren zusammengefasst sind. Der einzige Grund, explizit eine \book-Umgebung zu setzen, ist, wenn mehrere Ausgabedateien aus einer einzigen Quelldatei erstellt werden sollen. Eine Ausnahme sind lilypond-book-Dokumente, in denen eine \book-Umgebung explizit hinzugefügt werden muss, wenn mehr als eine \score- oder \markup-Umgebung im gleichen Beispiel angezeigt werden soll. Dieses Verhalten kann verändert werden, indem die Variable toplevel-book-handler auf höchster Ebene gesetzt wird. Die Definition des Standards findet sich in der Datei ‘../scm/lily.scm’.
Eine \bookpart-Umgebung. Ein Buch (\book) kann in mehrere Teile untergliedert sein, indem \bookpart-Umgebungen eingesetzt werden. Jeder Buchabschnitt beginnt auf einer neuen Seite und kann eigene Papierdefinitionen in einer \paper-Umgebung haben.
Ein zusammengesetzter musikalischer Ausdruck wie etwa
{ c'4 d' e'2 }
Dieses Beispiel wird von LilyPond automatisch in einer \score-Umgebung in einem Buch interpretiert und mit anderen \score-Umgebungen und musikalischen Ausdrücken auf der höchsten Ebene zusammen ausgegeben. Anders gesagt: eine Datei, die nur das obige Beispiel beinhaltet, wird übersetzt zu

\book {
  \score {
    \new Staff {
      \new Voice {
        { c'4 d' e'2 }
      }
    }
    \layout { }
  }
  \paper { }
  \header { }
}

Dieses Verhalten kann verändert werden, indem die Variable toplevel-music-handler auf der obersten Ebene gesetzt wird. Die Definition des Standards findet sich in der Datei ‘../scm/lily.scm’.

Eine Textbeschriftung, eine Strophe etwa:
\markup {
   2.  Die erste Zeile der zweiten Strophe.
}
Textbeschriftungen werden über, zwischen oder unter musikalischen Ausdrücken gesetzt, so wie sie notiert werde.

Eine Variable, wie
foo = { c4 d e d }
Sie kann dann später in der Datei eingesetzt werden, indem \foo geschrieben wird. Die Bezeichnung der Variable darf nur aus alphabetischen Zeichen bestehen, keine Zahlen, Unter- oder Bindestriche.

Das folgende Beispiel zeigt drei Dinge, die auf der obersten Ebene notiert werden können:

\layout {
  % Zeilen rechtsbündig setzen
  ragged-right = ##t
}

\header {
   title = "Do-re-mi"
}

{ c'4 d' e2 }
An einer beliebigen Stelle der Datei kann jede der folgenden lexikalen Anweisungen notiert werden:

\version
\include
\sourcefilename
\sourcefileline
Ein einzeiliger Kommentar, beginnend mit %.
Ein mehrzeiliger Kommentar, umgeben von %{ .. %}.

\version "2.24.4"

\header { }

\score {
   … zusammengesetzter Musik-Ausdruck …   % Die gesamten Noten kommen hier hin!
  \layout { }
  \midi { }
}
Zu diesem Muster gibt es viele Variationen, aber obiges Beispiel stellt eine hilfreiche Ausgangsbasis dar.

Bis jetzt hat keines der verwendeten Beispiele den \score{} Befehl gebraucht. Das liegt daran, dass LilyPond automatisch fehlende Befehle hinzufügt, sollten sie fehlen. Konkret betrachtet LilyPond eine Eingabe in der Form

\relative {
  c''4 a d c
}
als eine abgekürzte Form von

\book {
  \score {
    \new Staff {
      \new Voice {
        \relative {
          c''4 a b c
        }
      }
    }
    \layout { }
  }
}

Achtung: Wenn mehr als ein paar Zeilen an Musik eingegeben werden, empfiehlt es sich, die Notenzeilen und die Stimmen immer explizit mit \new Staff und \new Voice zu erzeugen.

Im Moment wollen wir aber zu unserem ersten Beispiel zurückkehren und nur den \score-Befehl näher betrachten.

Eine Partitur (\score) muss immer genau einen musikalischen Ausdruck enthalten. Es sei daran erinnert, dass ein musikalischer alles mögliche sein kann. Angefangen bei einer einzelnen Note bis hin zu riesigen zusammengesetzten Ausdrücken wie

{
  \new StaffGroup <<
     … füge hier die gesamte Partitur einer Wagner Oper ein …
  >>
}
Da sich alles innerhalb der geschweiften Klammern { … } befindet, wird es wie ein einziger musikalischer Ausdruck behandelt.

Wie wir oben schon gesehen haben kann der \score Block auch andere Dinge enthalten, wie etwa

\score {
  { c'4 a b c' }
  \header { }
  \layout { }
  \midi { }
}

melodie = \relative {
  c'4 a b c
}

\score {
  \melodie
}
Wenn LilyPond diese Datei analysiert, nimmt es den Inhalt von melodie (alles nach dem Gleichheitszeichen) und fügt ihn immer dann ein, wenn ein \melodie vorkommt. Die Namen sind frei wählbar, die Variable kann genauso gut melodie, GLOBAL, rechteHandKlavier, oder foofoobarbaz heißen. Als Variablenname kann fast jeder beliebige Name benutzt werden, allerdings dürfen nur Buchstaben vorkommen (also keine Zahlen, Unterstriche, Sonderzeichen, etc.) und er darf nicht wie ein LilyPond-Befehl lauten. Für mehr Information siehe Tipparbeit durch Variablen und Funktionen einsparen. Die genauen Einschränkungen sind


sopranoMusic = \relative { a'4 b c b8( a) }
altoMusic = \relative { e'4 e e f }
tenorMusic = \relative { c'4 b e d8( c) }
bassMusic = \relative { a4 gis a d, }
allLyrics = \lyricmode {King of glo -- ry }
<<
  \new Staff = "Soprano" \sopranoMusic
  \new Lyrics \allLyrics
  \new Staff = "Alto" \altoMusic
  \new Lyrics \allLyrics
  \new Staff = "Tenor" {
    \clef "treble_8"
    \tenorMusic
  }
  \new Lyrics \allLyrics
  \new Staff = "Bass" {
    \clef "bass"
    \bassMusic
  }
  \new Lyrics \allLyrics
  \new PianoStaff <<
    \new Staff = "RH" {
      \set Staff.printPartCombineTexts = ##f
      \partCombine
      \sopranoMusic
      \altoMusic
    }
    \new Staff = "LH" {
      \set Staff.printPartCombineTexts = ##f
      \clef "bass"
      \partCombine
      \tenorMusic
      \bassMusic
    }
  >>
>>
*/

/*
\score {
  \relative {
    c''1 \mark \markup { \char ##x03A8 }
    c1_\markup { \tiny { \char ##x03B1 " to " \char ##x03C9 } }
  }
  \addlyrics { O \markup { \concat { Ph \char ##x0153 be! } } }
}
\markup { "Copyright 2008--2022" \char ##x00A9 }

Um das Copyright-Zeichen zu notieren, kann folgender Code eingesetzt werden:

\header {
  copyright = \markup { \char ##x00A9 "2008" }
}

ASCII-Aliase
Eine Liste von ASCII-Befehlen für Sonderzeichen kann eingefügt werden:

\paper {
  #(include-special-characters)
}

\markup "&flqq; &ndash; &OE;uvre incomplète&hellip; &frqq;"

\score {
  \new Staff { \repeat unfold 9 a'4 }
  \addlyrics {
    This is al -- so wor -- kin'~in ly -- rics: &ndash;_&OE;&hellip;
  }
}

\markup \column {
  "The replacement can be disabled:"
  "&ndash; &OE; &hellip;"
  \override #'(replacement-alist . ()) "&ndash; &OE; &hellip;"
}
*/

/*
Notationsfragmente extrahieren
Es ist möglich, kleine Abschnitte einer großen Partitur direkt aus der Quelldatei zu erzeugen. Das kann damit verglichen werden, dass man mit der Schere bestimmte Regionen ausschneidet.

Es wird erreicht, indem man die Takte, die ausgeschnitten werden sollen (engl. to clip = ausschneiden), extra definiert. Mit folgender Definition beispielsweise

\layout {
  clip-regions
  = #(list
      (cons
       (make-rhythmic-location 5 1 2)
       (make-rhythmic-location 7 3 4)))
}
wird ein Fragment ausgeschnitten, dass auf der Mitte des fünften Taktes beginnt und im siebten Takt endet. Die Bedeutung von 5 1 2 ist: nach einer Halben in Takt fünf, 7 3 4 heißt: nach drei Vierteln in Takt 7.

Weitere Bereiche, die ausgeschnitten werden sollen, können definiert werden, indem mehrere derartige Paare definiert werden.

Um diese Funktion auch nutzen zu können, muss LilyPond mit dem Parameter ‘-dclip-systems’ aufgerufen werden. Die Schnipsel werden als EPS ausgegeben und dann zu PDF und PNG konvertiert, wenn diese Formate auch als Parameter angegeben werden.

Zu mehr Information über Ausgabeformate siehe lilypond aufrufen.

Korrigierte Musik überspringen
Wenn man Noten eingibt oder kopiert, sind meistens nur die Noten nahe dem Ende (wo gerade neue Noten notiert wurden) wichtig für Kontrolle und Korrektur. Um die Korrektur zu beschleunigen, kann eingestellt werden, dass nur die letzten paar Takte angezeigt werden. Das erreicht man mit dem Befehl

showLastLength = R1*5
\score { ... }
in der Quelldatei. Damit werden nur die letzten fünf Takte (in einem 4/4-Takt) eines jeden \score-Abschnitts übersetzt. Besonders bei längeren Stücken ist es meistens sehr viel schneller, nur einen kleinen Teil des Stückes zu setzen als die gesamte Länge. Wenn man am Anfang eines Stückes arbeitet (weil etwa ein neuer Teil hinzugefügt werden soll), kann auch die showFirstLength-Eigenschaft nützlich sein.

Nur bestimmte Teile einer Partitur zu überspringen, kann mit der Eigenschaft Score.skipTypesetting sehr genau kontrolliert werden. Für den Bereich, für den sie auf „wahr“ gesetzt wird, wird kein Notensatz ausgegeben.

Diese Eigenschaft kann auch benutzt werden, um die MIDI-Ausgabe zu kontrollieren. Hiermit werden alle Ereignisse, auch Tempo- und Instrumentenwechsel ausgelassen. Man muss also sehr genau darauf achten, dass nichts unerwartetes geschieht.

\relative {
  c''8 d
  \set Score.skipTypesetting = ##t
  e8 e e e e e e e
  \set Score.skipTypesetting = ##f
  c8 d b bes a g c2
}
*/

/*
Die Notationsschriftart verändern
Gonville ist eine Alternative zu der Emmentaler-Schriftart, die in LilyPond eingesetzt wird und kann unter der Adresse

http://www.chiark.greenend.org.uk/~sgtatham/gonville/
heruntergeladen werden.

Bekannte Probleme und Warnungen
Gonville kann nicht verwendet werden, um Alte Notation zu setzen und es ist wahrscheinlich, dass neuere Glyphen in späteren Versionen von LilyPond nicht in Gonville enthalten sein werden. Bitte lesen Sie die Webseite des Autoren zu mehr Information hierzu und zu anderen Einzelheiten, wie auch der Lizenz von Gonville.
*/

/*
für LYEDITOR entspricht dem normalen Aufruf von emacsclient.

Die point-and-click-Links vergrößern die Größe des PDFs sehr stark. Um die Größe von PDFs und auch PS-Dateien zu verkleinern, kann point and click ausgeschaltet werden, indem man in der Eingabedatei

\pointAndClickOff
schreibt. Point and click kann explizit aktiviert werden mit dem Befehl

\pointAndClickOn
Alternativ können Sie point and click auch mit einer Kommandozeilenoption anschalten:

lilypond -dno-point-and-click file.ly
Achtung: Sie sollten immer point and click ausschalten, wenn Sie LilyPond-Dateien verteilen wollen, damit keine Informationen über Ihre Dateistrukturen in den Dateien gespeichert werden, was ein Sicherheitsrisiko darstellen könnte.

Selektives point-and-click
Für einige interaktive Anwendungen kann es von Vorteil sein, nur einige Elemente mit Point and click zu aktivieren. Wenn man beispielsweise eine Anwendung erstellen will, die Audio oder Video beginnend von einer angeklickten Note abspielt, würde es unpraktisch sein, wenn die Point-and-click-Zeiger Information eines Bogens oder Versetzungszeichen, die gleichzeitig mit der Note erscheinen, darstellen würden.

Man kann dieses Verhalten erreichen, indem man angibt, welche Ereignisse aufgenommen werden sollen:

Direkt in der ‘.ly’-Datei:
\pointAndClickTypes #'note-event
\relative {
  c'2\f( f)
}
oder

#(ly:set-option 'point-and-click 'note-event)
\relative {
  c'2\f( f)
}
Auf der Kommandozeile:
lilypond -dpoint-and-click=note-event   example.ly
Auch mehrere Ereignisse können eingebunden werden:

Direkt in der ‘.ly’-Datei:
\pointAndClickTypes #'(note-event dynamic-event)
\relative {
  c'2\f( f)
}
oder

#(ly:set-option 'point-and-click '(note-event dynamic-event))
\relative {
  c'2\f( f)
}
Auf der Kommandozeile:

lilypond \
  -e"(ly:set-option 'point-and-click '(note-event dynamic-event))" \
  example.ly
*/

/*
LilyPond-Noten in andere Programme integrieren
Um die Ausgabe von LilyPond in anderen Programmen einzufügen, sollte lilypond anstelle von lilypond-book benutzt werden. Jedes Beispiel muss getrennt manuell erzeugt und ins Dokument eingefügt werden; für letzteres schlagen Sie bitte im Handbuch Ihrer Textverarbeitungs-Software nach. Die meisten Programme unterstützen das Einfügen von Grafiken im ‘PNG’-, ‘EPS’- oder ‘PDF’-Format.

Um den leeren Rand um die Notenzeilen zu verringern, können folgende Einstellungen benutzt werden:

\paper{
  indent=0\mm
  line-width=120\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

{ c1 }
Benutzbare Bilddateien können mit folgendem Befehl erzeugt werden:

EPS

lilypond -dbackend=eps -dno-gs-load-fonts -dinclude-eps-fonts   Dateiname.ly

PNG

lilypond -dbackend=eps -dno-gs-load-fonts -dinclude-eps-fonts --png Dateiname.ly

Ein transparentes PNG

lilypond -dbackend=eps -dno-gs-load-fonts -dinclude-eps-fonts \
  -dpixmap-format=pngalpha --png myfile.ly
*/
