package lilypond

import "bytes"

type Layout struct {
	elements []Element
}

func (l *Layout) Add(elms ...Element) *Layout {
	l.elements = append(l.elements, elms...)
	return l
}

func (l Layout) String() string {
	if len(l.elements) == 0 {
		return ""
	}
	var bf bytes.Buffer

	bf.WriteString(`\layout { ` + "\n")

	for _, elm := range l.elements {
		bf.WriteString(elm.String())
	}

	bf.WriteString("\n } \n")

	return bf.String()
}
