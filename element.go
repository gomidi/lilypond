package lilypond

import (
	"bytes"
	"fmt"
)

type Element interface {
	String() string
}

func ElementGroup(elms ...Element) Element {
	return elementGroup(elms)
}

type elementGroup []Element

func (g elementGroup) String() string {
	var bf bytes.Buffer

	for _, e := range g {
		bf.WriteString(e.String())
	}

	return bf.String()
}

type String string

func (s String) String() string {
	return string(s)
}

type Line string

func (s Line) String() string {
	return string(s) + "\n"
}

/*
https://lilypond.org/doc/v2.24/Documentation/notation/displaying-pitches#clef

Oktavierungsklammern
Oktavierungsklammern zeigen eine zusätzliche Transposition von einer Oktave an:

\relative a' {
  a2 b
  \ottava #-2
  a2 b
  \ottava #-1
  a2 b
  \ottava #0
  a2 b
  \ottava #1
  a2 b
  \ottava #2
  a2 b
}
*/

func Ottava(n int) Line {
	return Line(fmt.Sprintf(`\ottava #%v`, n))
}

// "Zeilenumbruch"
// \break

type breaK struct{}

func (b breaK) String() string {
	return fmt.Sprintf(`\break ` + "\n")
}

var Break = breaK{}

const StemDown = Line(`\stemDown`)
const StemUp = Line(`\stemUp`)

func InstrumentName(n string) Line {
	return Line(fmt.Sprintf("instrumentName = %q", n))
}

func ShortInstrumentName(n string) Line {
	return Line(fmt.Sprintf("shortInstrumentName = %q", n))
}
