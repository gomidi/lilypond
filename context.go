package lilypond

/*
Kontexte werden normalerweise am ersten musikalischen Moment beendet, an dem sie nichts mehr zu tun haben. Ein Voice-Kontext stirbt also sofort, wenn keine Ereignisse mehr auftreten, Staff-Kontexte sobald alle in ihnen enthaltenen Voice-Kontexte keine Ereignisse mehr aufweisen usw. Das kann Schwierigkeiten ergeben, wenn auf frühere Kontexte verwiesen werden soll, die in der Zwischenzeit schon gestorben sind, beispielsweise wenn man Systemwechsel mit \change-Befehlen vornimmt, wenn Gesangstext einer Stimme mit dem \lyricsto-Befehl zugewiesen wird oder wenn weitere musikalische Ereignisse zu einem früheren Kontext hinzugefügt werden sollen.

Es gibt eine Ausnahme dieser Regel: genau ein Voice-Kontext innerhalb eines Staff-Kontextes oder in einer <<...>>-Konstruktion bleibt immer erhalten bis zum Ende des Staff-Kontextes oder der <<...>>-Konstruktion, der ihn einschließt, auch wenn es Abschnitte gibt, in der er nichts zu tun hat. Der Kontext, der erhalten bleibt ist immer der erste, der in der ersten enthaltenden {...}-Konstruktion angetroffen wird, wobei <<...>>-Konstruktionen ignoriert werden.

Jeder Kontext kann am Leben gehalten werden, indem man sicherstellt dass er zu jedem musikalischen Moment etwas zu tun hat. Staff-Kontexte werden am Leben gehalten, indem man sicherstellt, dass eine der enthaltenen Stimmen am Leben bleibt. Eine Möglichkeit, das zu erreichen, ist es, unsichtbare Pause zu jeder Stimme hinzuzufügen, die am Leben gehalten werden soll. Wenn mehrere Stimmen sporadisch benutzt werden sollen, ist es am sichersten, sie alle am Leben zu halten und sich nicht auf die Ausnahmeregel zu verlassen, die im vorigen Abschnitt dargestellt wurde.

Im folgenden Beispiel werden sowohl Stimme A als auch B auf diese Weise für die gesamte Dauer des Stückes am Leben gehalten.

musicA = \relative { d''4 d d d }
musicB = \relative { g'4 g g g }
keepVoicesAlive = {
  <<
    \new Voice = "A" { s1*5 }  % Keep Voice "A" alive for 5 bars
    \new Voice = "B" { s1*5 }  % Keep Voice "B" alive for 5 bars
  >>
}

music = {
  \context Voice = "A" {
    \voiceOneStyle
    \musicA
  }
  \context Voice = "B" {
    \voiceTwoStyle
    \musicB
  }
  \context Voice = "A" { \musicA }
  \context Voice = "B" { \musicB }
  \context Voice = "A" { \musicA }
}

\score {
  \new Staff <<
    \keepVoicesAlive
    \music
  >>
}

*/
