package lilypond

import (
	"bytes"
	"fmt"
)

type Staffer interface {
	isStaff()
	Element
}

var _ Staffer = Staff{}
var _ Staffer = FretBoards{}
var _ Staffer = ChordNames{}
var _ Staffer = DrumStaff{}
var _ Staffer = TabStaff{}
var _ Staffer = StaffGroup{}
var _ Staffer = PianoStaff{}
var _ Staffer = GrandStaff{}
var _ Staffer = ChoirStaff{}

/*
Neue Notensysteme erstellen
Notensysteme (engl. staff, Pl. staves) werden mit dem \new oder \context-Befehl erstellt. Zu Einzelheiten siehe Kontexte erstellen und referenzieren.

Der einfachste Notensystem-Kontext ist Staff:

\new Staff \relative { c''4 d e f }
[image of music]

DrumStaff (Perkussionsnotensystem) erstellt ein Notensystem mit fünf Linien, das für ein typisches Schlagzeug eingerichtet ist. Für jedes Instrument werden unterschiedliche Symbole dargestellt. Die Instrumente werden innerhalb der drummode-Umgebung gesetzt, wo jedes Instrument seine eigene Bezeichnung hat. Zu Einzelheiten siehe Schlagzeugsysteme.

\new DrumStaff {
  \drummode { cymc hh ss tomh }
}
[image of music]

RhythmicStaff (Rhythmus-System) erstellt ein Notensystem mit nur einer Notenlinie, auf welcher nur die rhythmischen Werte der eingegebenen Noten dargestellt werden. Die wirklichen Längen bleiben erhalten. Zu Einzelheiten, siehe Melodierhythmus anzeigen.

TabStaff (Tabulatursystem) erstellt eine Tabulatur mit sechs Saiten in der üblichen Gitarrenstimmung. Zu Einzelheiten siehe Standardtabulaturen.

\new TabStaff \relative { c''4 d e f }

Systeme gruppieren
Es gibt verschiedene Kontexte, um einzelne Notensysteme zu gruppieren und einer Partitur zu verbinden. Jeder Gruppenstil beeinflusst das Aussehen des Systemanfangs und das Verhalten der Taktlinien.

Wenn kein Kontext angegeben ist, wird die Standardeinstellung eingesetzt: die Gruppe beginnt mit einer vertikalen Linie und die Taktlinien sind nicht verbunden.

<<
  \new Staff \relative { c''1 c }
  \new Staff \relative { c''1 c }
>>
[image of music]

Im StaffGroup-Kontext die Gruppe mit einer eckigen Klammer begonnen und die Taktlinien durch alle Systeme gezogen.

\new StaffGroup <<
  \new Staff \relative { c''1 c }
  \new Staff \relative { c''1 c }
>>

In einem ChoirStaff (Chorsystem) beginnt die Gruppe mit einer eckigen Klammer, aber die Taktlinien sind nicht verbunden.

\new ChoirStaff <<
  \new Staff \relative { c''1 c }
  \new Staff \relative { c''1 c }
>>
[image of music]

In einem GrandStaff (Akkolade) beginnt die Gruppe mit einer geschweiften Klammer und die Taktlinien sind durchgezogen.

\new GrandStaff <<
  \new Staff \relative { c''1 c }
  \new Staff \relative { c''1 c }
>>

Der PianoStaff-(Klaviersystem)-Kontext ist identisch mit dem GrandStaff-Kontext, aber es ermöglicht zusätzlich direkt die Angabe einer Instrumentbezeichnung. Zu Einzelheiten siehe Instrumentenbezeichnungen.

\new PianoStaff <<
  \set PianoStaff.instrumentName = "Piano"
  \new Staff \relative { c''1 c }
  \new Staff \relative { \clef bass c1 c }
>>

*/

type ChoirStaff struct {
	name     string
	staffers []Staffer
	with     []Element
}

func (l *ChoirStaff) SetName(name string) *ChoirStaff {
	l.name = name
	return l
}

func (v *ChoirStaff) With(s ...Element) *ChoirStaff {
	v.with = append(v.with, s...)
	return v
}

func (l *ChoirStaff) Add(st ...Staffer) *ChoirStaff {
	l.staffers = append(l.staffers, st...)
	return l
}

func (p ChoirStaff) isStaff() {}
func (p ChoirStaff) String() string {
	if len(p.staffers) == 0 {
		return ""
	}

	if len(p.staffers) == 1 {
		var bf bytes.Buffer

		if p.name != "" {
			fmt.Fprintf(&bf, `\new ChoirStaff = %q`, p.name)
		} else {
			bf.WriteString(`\new ChoirStaff`)
		}

		if len(p.with) > 0 {
			fmt.Fprintf(&bf, ` \with { `+"\n")

			for _, w := range p.with {
				bf.WriteString(w.String())
			}

			bf.WriteString("\n } \n")
		}

		bf.WriteString(" { \n")

		bf.WriteString(p.staffers[0].String())
		bf.WriteString("\n } \n")

		return bf.String()
	}

	var bf bytes.Buffer

	if p.name != "" {
		fmt.Fprintf(&bf, `\new ChoirStaff = %q`, p.name)
	} else {
		bf.WriteString(`\new ChoirStaff`)
	}

	if len(p.with) > 0 {
		fmt.Fprintf(&bf, ` \with { `+"\n")

		for _, w := range p.with {
			bf.WriteString(w.String())
		}

		bf.WriteString("\n } \n")
	}

	bf.WriteString(" << \n")

	for _, s := range p.staffers {
		bf.WriteString(s.String())
	}

	bf.WriteString("\n >> \n")

	return bf.String()

}

type FretBoards struct {
	elements []Element
	with     []Element
}

func (v *FretBoards) With(s ...Element) *FretBoards {
	v.with = append(v.with, s...)
	return v
}

func (v *FretBoards) Add(s ...Element) *FretBoards {
	v.elements = append(v.elements, s...)
	return v
}

func (v FretBoards) String() string {
	var bf bytes.Buffer

	fmt.Fprintf(&bf, `\new FretBoards `)

	if len(v.with) > 0 {
		fmt.Fprintf(&bf, ` \with { `+"\n")

		for _, w := range v.with {
			bf.WriteString(w.String())
		}

		bf.WriteString("\n } \n")
	}

	bf.WriteString(" { \n")

	bf.WriteString(`\chordmode { ` + "\n")

	for _, s := range v.elements {
		bf.WriteString(s.String())
	}

	bf.WriteString("\n } \n")
	bf.WriteString("\n } \n")

	return bf.String()
}

func (p FretBoards) isStaff() {}

type ChordNames struct {
	elements []Element
	with     []Element
}

func (v *ChordNames) With(s ...Element) *ChordNames {
	v.with = append(v.with, s...)
	return v
}

func (v *ChordNames) Add(s ...Element) *ChordNames {
	v.elements = append(v.elements, s...)
	return v
}

func (v ChordNames) String() string {
	var bf bytes.Buffer

	fmt.Fprintf(&bf, `\new ChordNames `)

	if len(v.with) > 0 {
		fmt.Fprintf(&bf, ` \with { `+"\n")

		for _, w := range v.with {
			bf.WriteString(w.String())
		}

		bf.WriteString("\n } \n")
	}

	bf.WriteString(" { \n")

	bf.WriteString(`\chordmode { ` + "\n")

	for _, s := range v.elements {
		bf.WriteString(s.String())
	}

	bf.WriteString("\n } \n")
	bf.WriteString("\n } \n")

	return bf.String()
}

func (p ChordNames) isStaff() {}

func (p TabStaff) isStaff() {}

type TabStaff struct {
	elements     []Element
	fullNotation bool
	with         []Element
}

func (v *TabStaff) With(s ...Element) *TabStaff {
	v.with = append(v.with, s...)
	return v
}

func (l *TabStaff) Add(elms ...Element) *TabStaff {
	l.elements = append(l.elements, elms...)
	return l
}

func (t *TabStaff) SetFullNotation() *TabStaff {
	t.fullNotation = true
	return t
}

func (t TabStaff) String() string {
	var bf bytes.Buffer

	bf.WriteString(`\new TabStaff`)

	if len(t.with) > 0 {
		fmt.Fprintf(&bf, ` \with { `+"\n")

		for _, w := range t.with {
			bf.WriteString(w.String())
		}

		bf.WriteString("\n } \n")
	}

	bf.WriteString(" { \n")

	if t.fullNotation {
		bf.WriteString(`\tabFullNotation ` + "\n")
	}

	for _, n := range t.elements {
		bf.WriteString(n.String())
	}

	bf.WriteString("\n } \n")

	return bf.String()
}

type StaffGroup struct {
	name     string
	staffers []Staffer
	with     []Element
	loose    bool
}

func (l *StaffGroup) Loose() *StaffGroup {
	if len(l.with) == 0 {
		l.loose = true
	}

	return l
}

func (l *StaffGroup) SetName(name string) *StaffGroup {
	l.name = name
	return l
}

func (v *StaffGroup) With(s ...Element) *StaffGroup {
	v.with = append(v.with, s...)
	v.loose = false
	return v
}

func (l *StaffGroup) Add(st ...Staffer) *StaffGroup {
	l.staffers = append(l.staffers, st...)
	return l
}

func (p StaffGroup) isStaff() {}
func (p StaffGroup) String() string {
	if len(p.staffers) == 0 {
		return ""
	}

	if len(p.staffers) == 1 {
		var bf bytes.Buffer

		if !p.loose {
			if p.name != "" {
				fmt.Fprintf(&bf, `\new StaffGroup = %q`, p.name)
			} else {
				bf.WriteString(`\new StaffGroup`)
			}

			if len(p.with) > 0 {
				fmt.Fprintf(&bf, ` \with { `+"\n")

				for _, w := range p.with {
					bf.WriteString(w.String())
				}

				bf.WriteString("\n } \n")
			}

			bf.WriteString(" { \n")
		}

		bf.WriteString(p.staffers[0].String())

		if !p.loose {
			bf.WriteString("\n } \n")
		}

		return bf.String()
	}

	var bf bytes.Buffer

	if !p.loose {
		if p.name != "" {
			fmt.Fprintf(&bf, `\new StaffGroup = %q`, p.name)
		} else {
			bf.WriteString(`\new StaffGroup`)
		}

		if len(p.with) > 0 {
			fmt.Fprintf(&bf, ` \with { `+"\n")

			for _, w := range p.with {
				bf.WriteString(w.String())
			}

			bf.WriteString("\n } \n")
		}
	}

	bf.WriteString(" << \n")

	for _, s := range p.staffers {
		bf.WriteString(s.String())
	}

	bf.WriteString("\n >> \n")

	return bf.String()

}

type GrandStaff struct {
	staffGroups []*StaffGroup
	with        []Element
}

func (v *GrandStaff) With(s ...Element) *GrandStaff {
	v.with = append(v.with, s...)
	return v
}

func (l *GrandStaff) Add(st ...*StaffGroup) *GrandStaff {
	l.staffGroups = append(l.staffGroups, st...)
	return l
}

var _ Staffer = GrandStaff{}

func (p GrandStaff) isStaff() {}
func (p GrandStaff) String() string {
	if len(p.staffGroups) == 0 {
		return ""
	}

	if len(p.staffGroups) == 1 {
		var bf bytes.Buffer

		bf.WriteString(`\new GrandStaff`)

		if len(p.with) > 0 {
			fmt.Fprintf(&bf, ` \with { `+"\n")

			for _, w := range p.with {
				bf.WriteString(w.String())
			}

			bf.WriteString("\n } \n")
		}

		bf.WriteString(" { \n")

		bf.WriteString(p.staffGroups[0].String())
		bf.WriteString("\n } \n")

		return bf.String()
	}

	var bf bytes.Buffer

	bf.WriteString(`\new GrandStaff`)

	if len(p.with) > 0 {
		fmt.Fprintf(&bf, ` \with { `+"\n")

		for _, w := range p.with {
			bf.WriteString(w.String())
		}

		bf.WriteString("\n } \n")
	}

	bf.WriteString(" << \n")

	for _, s := range p.staffGroups {
		bf.WriteString(s.String())
	}

	bf.WriteString("\n >> \n")

	return bf.String()
}

type PianoStaff struct {
	lh   *Staff
	rh   *Staff
	with []Element
}

func (v *PianoStaff) With(s ...Element) *PianoStaff {
	v.with = append(v.with, s...)
	return v
}

func (l *PianoStaff) Set(lh *Staff, rh *Staff) *PianoStaff {
	l.lh = lh
	l.rh = rh
	return l
}

func (p PianoStaff) isStaff() {}
func (p PianoStaff) String() string {
	var bf bytes.Buffer

	bf.WriteString(`\new PianoStaff`)

	if len(p.with) > 0 {
		fmt.Fprintf(&bf, ` \with { `+"\n")

		for _, w := range p.with {
			bf.WriteString(w.String())
		}

		bf.WriteString("\n } \n")
	}

	bf.WriteString(" << \n")

	bf.WriteString(p.lh.String())
	bf.WriteString(p.rh.String())
	bf.WriteString("\n >> \n")

	return bf.String()
}

func RhythmicStaff() *Staff {
	return &Staff{
		isRhythmicStaff: true,
	}
}

type Staff struct {
	name            string
	isRhythmicStaff bool
	voices          []*Voice
	with            []Element
}

func (v *Staff) With(s ...Element) *Staff {
	v.with = append(v.with, s...)
	return v
}

func (l *Staff) SetName(name string) *Staff {
	l.name = name
	return l
}

func (p *Staff) NewVoice() *Voice {
	v := &Voice{}
	p.voices = append(p.voices, v)
	return v
}

func (p Staff) isStaff() {}

func (p Staff) String() string {
	if len(p.voices) > 4 {
		panic("more than 4 voices is not supported")
	}
	//fmt.Println("Staff.String called")
	if len(p.voices) == 0 {
		//panic("no voices")
		return ""
	}

	var bf bytes.Buffer

	tname := "Staff"

	if p.isRhythmicStaff {
		tname = "RhythmicStaff"
	}

	if p.name != "" {
		fmt.Fprintf(&bf, `\new %s = %q`, tname, p.name)
	} else {
		fmt.Fprintf(&bf, `\new %s`, tname)
	}

	if len(p.with) > 0 {
		fmt.Fprintf(&bf, ` \with { `+"\n")

		for _, w := range p.with {
			bf.WriteString(w.String())
		}

		bf.WriteString("\n } \n")
	}

	bf.WriteString(" << \n")

	if len(p.voices) > 1 {
		for i, v := range p.voices {
			switch i {
			case 0:
				v.voiceName = `voiceOne`
			case 1:
				v.voiceName = `voiceTwo`
			case 2:
				v.voiceName = `voiceThree`
			case 3:
				v.voiceName = `voiceFour`
			default:
				panic("more than 4 voices is not supported")
			}
		}
	}

	for _, v := range p.voices {
		bf.WriteString(v.String())
	}

	bf.WriteString("\n >> \n")

	return bf.String()
}
